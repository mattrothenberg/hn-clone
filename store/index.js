import Vue from "vue";
import Vuex from "vuex";

const FRONT_PAGE = "/api/search?tags=front_page";
const itemEndpoint = id => `/api/items/${id}`;

const createStore = () => {
  return new Vuex.Store({
    state: {
      posts: {}
    },
    getters: {
      posts: state => state.posts,
      loadingPosts: state => Object.keys(state.posts).length === 0,
      postById: state => id => state.posts[id]
    },
    mutations: {
      SET_POSTS(state, posts) {
        state.posts = {};
        posts.forEach(post => {
          post.id = post.objectID;
          Vue.set(state.posts, post.objectID, post);
        });
      },
      SET_POST(state, post) {
        Vue.set(state.posts, post.id, post);
      }
    },
    actions: {
      async fetchPosts({ commit }) {
        const { hits } = await this.$axios.$get(FRONT_PAGE);
        commit("SET_POSTS", hits);
      },
      async fetchPost({ commit }, id) {
        const post = await this.$axios.$get(itemEndpoint(id));
        commit("SET_POST", post);
      }
    }
  });
};

export default createStore;
