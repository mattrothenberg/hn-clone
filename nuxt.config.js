const tailwindcss = require("tailwindcss");
const PurgecssPlugin = require("purgecss-webpack-plugin");
const glob = require("glob-all");
const path = require("path");

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: "HackerNews",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "BlueVoyant Coding Challenge"
      }
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  css: ["~/assets/css/main.css"],
  /*
  ** Customize the progress bar color
  */
  loading: { color: "#ff6600" },
  plugins: ["~/plugins/humanize", "~/plugins/pluralize"],
  modules: ["@nuxtjs/axios"],
  router: {
    scrollBehavior: function(to, from, savedPosition) {
      return { x: 0, y: 0 };
    }
  },
  axios: {
    proxy: true
  },
  proxy: {
    "/api": {
      target: "https://hn.algolia.com/api/v1/",
      pathRewrite: { "^/api/": "" }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    postcss: [tailwindcss("./tailwind.js"), require("autoprefixer")],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
      if (!isDev) {
        class TailwindExtractor {
          static extract(content) {
            return content.match(/[A-z0-9-:\/]+/g) || [];
          }
        }

        config.plugins.push(
          new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, "./pages/**/*.vue"),
              path.join(__dirname, "./layouts/**/*.vue"),
              path.join(__dirname, "./components/**/*.vue")
            ]),
            extractors: [
              {
                extractor: TailwindExtractor,
                extensions: ["html", "js", "vue"]
              }
            ],
            whitelistPatterns: [
              /html/,
              /body/,
              /__layout/,
              /__nuxt/,
              /nuxt-progress/,
              /page-(.*)/,
              /flip-list-(.*)/
            ]
          })
        );
      }
    }
  }
};
