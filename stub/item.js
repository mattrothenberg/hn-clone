export default {
  id: 16901599,
  created_at: "2018-04-23T08:36:22.000Z",
  created_at_i: 1524472582,
  type: "story",
  author: "Yossi_Frenkel",
  title: "Why it took a long time to build the tiny link preview on Wikipedia",
  url:
    "https://blog.wikimedia.org/2018/04/20/why-it-took-a-long-time-to-build-that-tiny-link-preview-on-wikipedia/",
  text: null,
  points: 628,
  parent_id: null,
  story_id: null,
  children: [
    {
      id: 16911617,
      created_at: "2018-04-24T11:25:50.000Z",
      created_at_i: 1524569150,
      type: "comment",
      author: "nonbel",
      title: null,
      url: null,
      text:
        '\u003cp\u003e\u0026gt;\u003ci\u003e\u0026quot;Through our analysis, we wanted to study user behavior towards the Page Previews feature to answer the following questions:\u003c/p\u003e\u003cp\u003eHow often do people use the feature to preview other pages? We set a threshold of 1000ms (one second) for a preview card to stay open before we counted it as viewed.\u003c/p\u003e\u003cp\u003eHow often do people disable the feature? It can be disabled by clicking on a settings icon at the bottom of each preview. A high rate in disabling Page Previews would indicate that this is not a feature users want. A low rate indicates users like the feature and would continue using it.\u0026quot;\u003c/i\u003e\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews\u0026#x2F;2017-18_A\u0026#x2F;B_Tests" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews\u0026#x2F;2017-18_A\u0026#x2F;B_Tes...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eMy experience was that suddenly something popped up over what I was trying to read so I was confused for a moment (hitting the first metric), but I did not notice the settings icon or expect I would be able to so easily turn these off (thus not hitting the second metric). After reading this I did turn it off.\u003c/p\u003e\u003cp\u003eI am definitely not at all a fan of this feature yet would be counted as positive by two different metrics.\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16902404,
      created_at: "2018-04-23T11:30:16.000Z",
      created_at_i: 1524483016,
      type: "comment",
      author: "Artemis2",
      title: null,
      url: null,
      text:
        '\u003cp\u003eLearned from this article that they make their Grafana dashboards public: \u003ca href="https:\u0026#x2F;\u0026#x2F;grafana.wikimedia.org\u0026#x2F;dashboard\u0026#x2F;file\u0026#x2F;varnish-http-errors.json?from=now-12h\u0026amp;to=now" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;grafana.wikimedia.org\u0026#x2F;dashboard\u0026#x2F;file\u0026#x2F;varnish-http-er...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eThe variety of metrics and the sheer volume of those is awesome to explore! (e.g. frequent peaks to 20M requests per minute)\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16903423,
          created_at: "2018-04-23T13:59:36.000Z",
          created_at_i: 1524491976,
          type: "comment",
          author: "ckoerner",
          title: null,
          url: null,
          text:
            '\u003cp\u003eWe\u0026#x27;re very transparent (I work for the Wikimedia Foundation).\u003c/p\u003e\u003cp\u003eYou can see the specific dashboard for the Page Preview feature here (Last 6 hours):\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;grafana.wikimedia.org\u0026#x2F;dashboard\u0026#x2F;db\u0026#x2F;eventlogging-schema-jumbo?orgId=1\u0026amp;var-datasource=eqiad%20prometheus%2Fops\u0026amp;var-schema=VirtualPageView\u0026amp;from=now-6h\u0026amp;to=now" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;grafana.wikimedia.org\u0026#x2F;dashboard\u0026#x2F;db\u0026#x2F;eventlogging-sche...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16902404,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16907786,
      created_at: "2018-04-23T21:51:05.000Z",
      created_at_i: 1524520265,
      type: "comment",
      author: "mewmew",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThis is great actually! Thanks a lot for implementing a quick preview feature, perfect for reading an article in one sitting without getting distrcted with side tabs.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16903952,
      created_at: "2018-04-23T14:59:23.000Z",
      created_at_i: 1524495563,
      type: "comment",
      author: "nfoz",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI dislike the \u0026quot;popup\u0026quot; UI metaphor, where some content obscures other content from view.  I also dislike when passive action such as \u0026quot;hover\u0026quot; causes stuff to jump around the screen.  I think these are distracting and confusing.  It makes me cautious for where can I \u0026quot;rest\u0026quot; my mouse on the screen.\u003c/p\u003e\u003cp\u003eI worry about the impact for accessibility-oriented users.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16908821,
      created_at: "2018-04-24T00:34:59.000Z",
      created_at_i: 1524530099,
      type: "comment",
      author: "8bitsrule",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThose summaries are a -big win-. Just hovering over an unfamiliar term gives you enough to get the gist. Greatly improves the value of the hyperlinks, without any injury to their utility if you want more. \u003ci\u003eThis should spread...\u003c/i\u003e\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16908567,
      created_at: "2018-04-23T23:56:48.000Z",
      created_at_i: 1524527808,
      type: "comment",
      author: "ryanpcmcquen",
      title: null,
      url: null,
      text: "\u003cp\u003eYak shaving?\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16903969,
      created_at: "2018-04-23T15:00:44.000Z",
      created_at_i: 1524495644,
      type: "comment",
      author: "abalone",
      title: null,
      url: null,
      text:
        "\u003cp\u003eWhy so much effort on a feature that doesn’t and probably can’t work on mobile?\u003c/p\u003e\u003cp\u003eWere they happy enough with Safari’s 3D Touch preview which does more or less exactly this? (Only with a full screen preview so they don’t have to get into the messy business of summarizing pages.)\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16910114,
          created_at: "2018-04-24T05:37:38.000Z",
          created_at_i: 1524548258,
          type: "comment",
          author: "PhasmaFelis",
          title: null,
          url: null,
          text:
            "\u003cp\u003eIf you don\u0026#x27;t see any reason for a feature that benefits better than 50% of your users (according to the chart you posted), I\u0026#x27;m really not sure what to tell you.\u003c/p\u003e",
          points: null,
          parent_id: 16903969,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16904233,
          created_at: "2018-04-23T15:26:44.000Z",
          created_at_i: 1524497204,
          type: "comment",
          author: "buildawesome",
          title: null,
          url: null,
          text:
            "\u003cp\u003eI imagine it\u0026#x27;s because they see high readership on desktop browsers.\u003c/p\u003e",
          points: null,
          parent_id: 16903969,
          story_id: 16901599,
          children: [
            {
              id: 16905300,
              created_at: "2018-04-23T17:07:52.000Z",
              created_at_i: 1524503272,
              type: "comment",
              author: "abalone",
              title: null,
              url: null,
              text:
                '\u003cp\u003eJust look at the traffic analysis. Mobile browsers have just about eclipsed desktop and they continue to grow.[1]\u003c/p\u003e\u003cp\u003eMobile support is table stakes for major features these days.\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;analytics.wikimedia.org\u0026#x2F;dashboards\u0026#x2F;browsers\u0026#x2F;#all-sites-by-os" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;analytics.wikimedia.org\u0026#x2F;dashboards\u0026#x2F;browsers\u0026#x2F;#all-sit...\u003c/a\u003e\u003c/p\u003e',
              points: null,
              parent_id: 16904233,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16906230,
          created_at: "2018-04-23T18:42:55.000Z",
          created_at_i: 1524508975,
          type: "comment",
          author: "always_good",
          title: null,
          url: null,
          text:
            "\u003cp\u003eI don\u0026#x27;t get it. Because only a sizable percentage of visits come from desktop browsers instead of most\u0026#x2F;all of them, then it doesn\u0026#x27;t make sense to improve things for them?\u003c/p\u003e\u003cp\u003eBizarre reasoning.\u003c/p\u003e",
          points: null,
          parent_id: 16903969,
          story_id: 16901599,
          children: [
            {
              id: 16906928,
              created_at: "2018-04-23T20:07:55.000Z",
              created_at_i: 1524514075,
              type: "comment",
              author: "abalone",
              title: null,
              url: null,
              text:
                '\u003cp\u003eYou\u0026#x27;ve misrepresented the reasoning. The best practice is: if you\u0026#x27;re going to tackle a major web UI problem, you should choose an approach that works for mobile (including iPads), which is already about half the traffic to wikipedia and growing. Especially if you are going to invest \u003ci\u003eyears\u003c/i\u003e of effort.\u003c/p\u003e\u003cp\u003eCalling that reasoning \u0026quot;bizarre\u0026quot; is just being deliberately obtuse.\u003c/p\u003e\u003cp\u003eTo wit, some browser vendors have already started recognizing this problem and solving it in a more general, mobile-compatible way.[1][2]\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;appleinsider.com\u0026#x2F;articles\u0026#x2F;15\u0026#x2F;04\u0026#x2F;28\u0026#x2F;os-x-tips-preview-links-in-safari-with-a-three-finger-tap" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;appleinsider.com\u0026#x2F;articles\u0026#x2F;15\u0026#x2F;04\u0026#x2F;28\u0026#x2F;os-x-tips-preview...\u003c/a\u003e\u003c/p\u003e\u003cp\u003e[2] \u003ca href="http:\u0026#x2F;\u0026#x2F;www.idownloadblog.com\u0026#x2F;2016\u0026#x2F;01\u0026#x2F;07\u0026#x2F;8-cool-ways-you-can-be-more-productive-in-safari-with-3d-touch\u0026#x2F;" rel="nofollow"\u003ehttp:\u0026#x2F;\u0026#x2F;www.idownloadblog.com\u0026#x2F;2016\u0026#x2F;01\u0026#x2F;07\u0026#x2F;8-cool-ways-you-can-...\u003c/a\u003e\u003c/p\u003e',
              points: null,
              parent_id: 16906230,
              story_id: 16901599,
              children: [
                {
                  id: 16907352,
                  created_at: "2018-04-23T21:01:40.000Z",
                  created_at_i: 1524517300,
                  type: "comment",
                  author: "always_good",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eThat is not a best practice when there are fundamental differences between mouse vs touch.\u003c/p\u003e\u003cp\u003eIn fact, trying to smooth over both desktop and mobile experiences with the exact same UI brush is the main reason we\u0026#x27;re left with the worst of both worlds.\u003c/p\u003e\u003cp\u003eBy the way, Facebook also has hover previews on desktop. :)\u003c/p\u003e\u003cp\u003eAlso, force touch OSX UI isn\u0026#x27;t a replacement when you need to manually highlight multi-word selections for it to work. Wikipedia\u0026#x27;s and Facebook\u0026#x27;s previews don\u0026#x27;t require you to do this.\u003c/p\u003e\u003cp\u003eI don\u0026#x27;t recommend sitting around and hoping browser vendors solve your problems. They\u0026#x27;re stuck in one-size-fits-all world while you can develop custom solutions for your site and users.\u003c/p\u003e",
                  points: null,
                  parent_id: 16906928,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16907608,
                      created_at: "2018-04-23T21:29:45.000Z",
                      created_at_i: 1524518985,
                      type: "comment",
                      author: "abalone",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003e\u003ci\u003e\u0026gt; trying to smooth over both desktop and mobile experiences with the exact same UI brush is the main reason we\u0026#x27;re left with the worst of both worlds.\u003c/i\u003e\u003c/p\u003e\u003cp\u003eExactly the opposite is true. Browser vendors are able to customize the solution to the device and leverage new gestures. Case in point: Safari on desktop (3 finger tap) vs. mobile (3D touch). Even within page content, responsive design techniques can customize it to different devices.\u003c/p\u003e\u003cp\u003e\u003ci\u003e\u0026gt; Also, force touch OSX UI isn\u0026#x27;t a replacement when you need to manually highlight multi-word selections for it to work.\u003c/i\u003e\u003c/p\u003e\u003cp\u003eThere is no such thing as force touch on OSX (macOS). And we are talking about hyperlink previews, not selections. It sounds like you\u0026#x27;re a little confused here.\u003c/p\u003e",
                      points: null,
                      parent_id: 16907352,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16906535,
      created_at: "2018-04-23T19:20:28.000Z",
      created_at_i: 1524511228,
      type: "comment",
      author: "erkose",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI commend Wikipedia for providing a configuration option to disable this feature. Netflix could learn a lot from Wikipedia.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901942,
      created_at: "2018-04-23T09:52:06.000Z",
      created_at_i: 1524477126,
      type: "comment",
      author: "second_sytem",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI hate those previews.  It is much faster and less distracting to actually follow a link and do some speed reading.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16903223,
      created_at: "2018-04-23T13:32:35.000Z",
      created_at_i: 1524490355,
      type: "comment",
      author: "joelcornett",
      title: null,
      url: null,
      text:
        '\u003cp\u003eI wrote this as one of my first HTML\u0026#x2F;JavaScript projects. It’s far from perfect, but it served me well for 4 years. It was not difficult to make and uses MediaWikis existing APIs.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;chrome.google.com\u0026#x2F;webstore\u0026#x2F;detail\u0026#x2F;wikipreview\u0026#x2F;iioncmbmegjiadkijjkmockilogpplhg" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;chrome.google.com\u0026#x2F;webstore\u0026#x2F;detail\u0026#x2F;wikipreview\u0026#x2F;iioncm...\u003c/a\u003e\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901705,
      created_at: "2018-04-23T08:56:06.000Z",
      created_at_i: 1524473766,
      type: "comment",
      author: "johnchristopher",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI remember seeing those preview for weeks (months) but the article publication date is 20th of April.\u003c/p\u003e\u003cp\u003eIt\u0026#x27;s pretty cool :).\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902532,
          created_at: "2018-04-23T11:48:45.000Z",
          created_at_i: 1524484125,
          type: "comment",
          author: "mavidser",
          title: null,
          url: null,
          text:
            '\u003cp\u003eI too was surprised by the date, given that I\u0026#x27;ve been using it for the past year, at least, or more.\u003c/p\u003e\u003cp\u003eApparently, it went into beta in 2014[1].\u003c/p\u003e\u003cp\u003e[1]: \u003ca href="https:\u0026#x2F;\u0026#x2F;blog.wikimedia.org\u0026#x2F;2014\u0026#x2F;03\u0026#x2F;26\u0026#x2F;Hovercards-now-available-as-a-beta-feature-on-all-wikimedia-wikis\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;blog.wikimedia.org\u0026#x2F;2014\u0026#x2F;03\u0026#x2F;26\u0026#x2F;Hovercards-now-availab...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901705,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16903756,
      created_at: "2018-04-23T14:40:00.000Z",
      created_at_i: 1524494400,
      type: "comment",
      author: "thelastidiot",
      title: null,
      url: null,
      text:
        "\u003cp\u003eNot to criticize the hard work that went into doing this feature (I worked on a project using wikipedia\u0026#x2F;wiktionary data), all the things that had to be achieved to come up with a \u0026quot;simple\u0026quot; preview features are just made hard because the data in wiki media is not machine friendly. Things like the obvious priority order of fields and bizarre templates that one needs to implement to parse the data makes the job unbelievably hard in the first place.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16904035,
          created_at: "2018-04-23T15:07:58.000Z",
          created_at_i: 1524496078,
          type: "comment",
          author: "squeaky-clean",
          title: null,
          url: null,
          text:
            "\u003cp\u003eYeah one of my first web scraping projects was using Wikipedia because I figured it would be easy to parse and have a fairly standard format, right? Well at least it was a good and sobering first lesson about cleaning data.\u003c/p\u003e",
          points: null,
          parent_id: 16903756,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16905673,
          created_at: "2018-04-23T17:47:47.000Z",
          created_at_i: 1524505667,
          type: "comment",
          author: "osteele",
          title: null,
          url: null,
          text:
            "\u003cp\u003eIn UCG gardens — as with data structure and algorithm design — there are trade-offs among retrieval difficulty (friction, for humans; time complexity, for machines), update difficulty, centralization and skill set of contributors, and centralization and skill set of editors, and the complexity of the structures themselves.\u003c/p\u003e\u003cp\u003eIMDB, CYC, Wolfram, and various RDF data sets, sample this space differently, and have different amounts of data and richness, probably as a result.\u003c/p\u003e",
          points: null,
          parent_id: 16903756,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902230,
      created_at: "2018-04-23T10:59:31.000Z",
      created_at_i: 1524481171,
      type: "comment",
      author: "omegote",
      title: null,
      url: null,
      text:
        "\u003cp\u003eNot to mention another probable reason: Mediawiki\u0026#x27;s codebase is a mess and should be rebuilt from the ground up, if possible not in PHP. I once had to build an extension for it and it still gives me the creeps.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902357,
          created_at: "2018-04-23T11:23:37.000Z",
          created_at_i: 1524482617,
          type: "comment",
          author: "lucideer",
          title: null,
          url: null,
          text:
            "\u003cp\u003e\u0026gt; \u003ci\u003eif possible not in PHP\u003c/i\u003e\u003c/p\u003e\u003cp\u003eI\u0026#x27;m not a fan of PHP as a language, but given the community has been working with PHP for 16 years, it would be odd to switch suddenly to an entirely new language and expect support and adoption.\u003c/p\u003e\u003cp\u003eThe codebase is an absolute nightmare though, and a ground-up rebuild would be great. I wonder though about it having a similar affect to the Wordpress codebase: people who recognise the mess stay away completely, and people who don\u0026#x27;t contribute, leaving a contributor base who isn\u0026#x27;t really equipped (or extremely willing) to do a quality, maintainable rewrite.\u003c/p\u003e\u003cp\u003eI suspect any rewrite attempt would be doomed to end up being an unmaintainable behemoth.\u003c/p\u003e\u003cp\u003eA better approach would be to focus on secure integration tools and API entry-points, to make users less entrenched and solely dependent on the MW software.\u003c/p\u003e",
          points: null,
          parent_id: 16902230,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16901721,
      created_at: "2018-04-23T08:58:31.000Z",
      created_at_i: 1524473911,
      type: "comment",
      author: "kabes",
      title: null,
      url: null,
      text:
        "\u003cp\u003eOr they could have started with an MVP without thumbnails and html and improved it over time, would have been more valuable then having no preview at all.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16901760,
          created_at: "2018-04-23T09:06:58.000Z",
          created_at_i: 1524474418,
          type: "comment",
          author: "SiempreViernes",
          title: null,
          url: null,
          text:
            "\u003cp\u003eThey do say they needed html to provide even a non-broken preview, and I expect the text summary was the most tricky thing  to do in any case, just guessing a good picture can\u0026#x27;t have been too hard.\u003c/p\u003e\u003cp\u003eIn any case, they had something 2 years ago but it was blocked by the community for not being good enough.\u003c/p\u003e",
          points: null,
          parent_id: 16901721,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902489,
      created_at: "2018-04-23T11:41:02.000Z",
      created_at_i: 1524483662,
      type: "comment",
      author: "mezzode",
      title: null,
      url: null,
      text:
        '\u003cp\u003eI opted into the beta for this way back and have been using it for ages, it was pretty surprising finding out it only just went into wide release. Given how useful I found it I\u0026#x27;d have thought it would have been released pretty quickly even when not perfect, but given the results can\u0026#x27;t complain.\n\u003ca href="https:\u0026#x2F;\u0026#x2F;blog.wikimedia.org\u0026#x2F;2014\u0026#x2F;03\u0026#x2F;26\u0026#x2F;Hovercards-now-available-as-a-beta-feature-on-all-wikimedia-wikis\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;blog.wikimedia.org\u0026#x2F;2014\u0026#x2F;03\u0026#x2F;26\u0026#x2F;Hovercards-now-availab...\u003c/a\u003e\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901725,
      created_at: "2018-04-23T08:59:58.000Z",
      created_at_i: 1524473998,
      type: "comment",
      author: "JorgeGT",
      title: null,
      url: null,
      text:
        "\u003cp\u003eTL;DR: they had to choose a thumbnail and summary. I have been using WikiWand for years, which not only did that but also makes reading Wikipedia much better. Maybe I\u0026#x27;m the only one but I have \u003ci\u003ethe hardest\u003c/i\u003e time reading paragraphs with lots of words per line. Previously I had to resize the browser each time I had to read something in Wikipedia :|\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16903472,
          created_at: "2018-04-23T14:06:11.000Z",
          created_at_i: 1524492371,
          type: "comment",
          author: "jaysonelliot",
          title: null,
          url: null,
          text:
            "\u003cp\u003eCame here to say the same thing about Wikiwand.\u003c/p\u003e\u003cp\u003eI\u0026#x27;ve been using it about a year now, and it has radically improved my experience reading Wikipedia.\u003c/p\u003e\u003cp\u003eAs you point out, it\u0026#x27;s had a smoothly working link preview feature all along, as well as a beautiful, usable reading experience. Not flashy or over-designed, just clean and elegant. I get a little caught off-guard now when I see a Wikipedia page without it.\u003c/p\u003e",
          points: null,
          parent_id: 16901725,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16903798,
          created_at: "2018-04-23T14:44:34.000Z",
          created_at_i: 1524494674,
          type: "comment",
          author: "fenwick67",
          title: null,
          url: null,
          text:
            "\u003cp\u003eYup, I have a user stylesheet that limits wikipedia\u0026#x27;s article width for this reason\u003c/p\u003e",
          points: null,
          parent_id: 16901725,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16902033,
          created_at: "2018-04-23T10:11:44.000Z",
          created_at_i: 1524478304,
          type: "comment",
          author: "DeGi",
          title: null,
          url: null,
          text:
            "\u003cp\u003eThanks for letting me know about WikiWand.\u003c/p\u003e",
          points: null,
          parent_id: 16901725,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902248,
      created_at: "2018-04-23T11:02:17.000Z",
      created_at_i: 1524481337,
      type: "comment",
      author: "vermontdevil",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI hate that preview feature. I always hover over links out of habit ready to click if I’m interested.  But that pop up blocks me from reading the text.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901745,
      created_at: "2018-04-23T09:04:17.000Z",
      created_at_i: 1524474257,
      type: "comment",
      author: "cantrevealname",
      title: null,
      url: null,
      text:
        "\u003cp\u003eOne downside is that when I move the cursor along while reading an article all sorts of links now pop up at me. What I mean is using the mouse or trackpad to keep my place in the article; sometimes I drag the cursor to highlight the text, especially when skimming. Surely I\u0026#x27;m not the only one who does this?\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16901993,
          created_at: "2018-04-23T10:02:34.000Z",
          created_at_i: 1524477754,
          type: "comment",
          author: "rplnt",
          title: null,
          url: null,
          text:
            "\u003cp\u003eThe bigger issue in my opinion, at least from the point that it\u0026#x27;s the wrong behavior 10\u0026#x2F;10 times, is that these previews pop up when you are just scrolling. Obviously I did not want to read a preview when page under my cursor changes. There should be a check if the cursor had actually moved.\u003c/p\u003e",
          points: null,
          parent_id: 16901745,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16901770,
          created_at: "2018-04-23T09:10:04.000Z",
          created_at_i: 1524474604,
          type: "comment",
          author: "throwawayReply",
          title: null,
          url: null,
          text:
            "\u003cp\u003eYou\u0026#x27;re very much not alone, this comes up frequently here at hn, because other sites also frustratingly block this behaviour often by popping up \u0026quot;tweet this\u0026quot; links after highlighting.\u003c/p\u003e",
          points: null,
          parent_id: 16901745,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16901789,
          created_at: "2018-04-23T09:14:59.000Z",
          created_at_i: 1524474899,
          type: "comment",
          author: "thinkingemote",
          title: null,
          url: null,
          text:
            "\u003cp\u003eAt the bottom of every popup there is a cog icon. Clicking the cog icon gives a popup where you can choose to Disable and Enable the feature.\u003c/p\u003e",
          points: null,
          parent_id: 16901745,
          story_id: 16901599,
          children: [
            {
              id: 16902267,
              created_at: "2018-04-23T11:06:23.000Z",
              created_at_i: 1524481583,
              type: "comment",
              author: "vermontdevil",
              title: null,
              url: null,
              text: "\u003cp\u003eThanks. I’ll do that!\u003c/p\u003e",
              points: null,
              parent_id: 16901789,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16901791,
          created_at: "2018-04-23T09:15:20.000Z",
          created_at_i: 1524474920,
          type: "comment",
          author: "stedaniels",
          title: null,
          url: null,
          text:
            "\u003cp\u003eThere should really be some \u0026quot;hover intent\u0026quot; going on if there isn\u0026#x27;t already and it just needs tweaking maybe?\u003c/p\u003e",
          points: null,
          parent_id: 16901745,
          story_id: 16901599,
          children: [
            {
              id: 16903378,
              created_at: "2018-04-23T13:52:51.000Z",
              created_at_i: 1524491571,
              type: "comment",
              author: "ckoerner",
              title: null,
              url: null,
              text:
                "\u003cp\u003eIIRC the delay value is set at 500ms before a preview is shown.\u003c/p\u003e",
              points: null,
              parent_id: 16901791,
              story_id: 16901599,
              children: [
                {
                  id: 16905634,
                  created_at: "2018-04-23T17:42:54.000Z",
                  created_at_i: 1524505374,
                  type: "comment",
                  author: "dc3k",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eIt seems like the timer starts as soon as the pointer enters the link area. I would be happier if it was 250ms that starts when the pointer stops moving.\u003c/p\u003e\u003cp\u003eEither way I love this feature.\u003c/p\u003e",
                  points: null,
                  parent_id: 16903378,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16908035,
                      created_at: "2018-04-23T22:24:02.000Z",
                      created_at_i: 1524522242,
                      type: "comment",
                      author: "sushid",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eI\u0026#x27;d imagine at the scale Wikipedia is operating, they\u0026#x27;d have to be sensitive about accessibility features. Some might find it hard to keep the pointer at a compete stop and the 500ms timer seems to do the job pretty well.\u003c/p\u003e",
                      points: null,
                      parent_id: 16905634,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16901752,
      created_at: "2018-04-23T09:05:08.000Z",
      created_at_i: 1524474308,
      type: "comment",
      author: "vanillaboy",
      title: null,
      url: null,
      text:
        "\u003cp\u003eCan I access this API externally, e.g. by sending a http get request to some endpoint, to get the summary content in JSON format?\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902007,
          created_at: "2018-04-23T10:05:51.000Z",
          created_at_i: 1524477951,
          type: "comment",
          author: "Findus23",
          title: null,
          url: null,
          text:
            '\u003cp\u003eYes you can, there is a nice API returning the Popup-data in JSON\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;stackoverflow.com\u0026#x2F;a\u0026#x2F;48527419\u0026#x2F;4398037" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;stackoverflow.com\u0026#x2F;a\u0026#x2F;48527419\u0026#x2F;4398037\u003c/a\u003e\u003c/p\u003e\u003cp\u003eDocumentation: \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;api\u0026#x2F;rest_v1\u0026#x2F;#!\u0026#x2F;Page_content\u0026#x2F;get_page_summary_title" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;api\u0026#x2F;rest_v1\u0026#x2F;#!\u0026#x2F;Page_content\u0026#x2F;get_pag...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901752,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16901855,
          created_at: "2018-04-23T09:30:34.000Z",
          created_at_i: 1524475834,
          type: "comment",
          author: "saagarjha",
          title: null,
          url: null,
          text:
            '\u003cp\u003eYou can get a similar effect by hitting Wikipedia for its extract. For example: \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=extracts\u0026amp;format=json\u0026amp;exintro=\u0026amp;titles=wikipedia" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=extract...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901752,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902011,
      created_at: "2018-04-23T10:06:49.000Z",
      created_at_i: 1524478009,
      type: "comment",
      author: "Findus23",
      title: null,
      url: null,
      text:
        '\u003cp\u003eIf you want to integrate this into your project: There is a nice API returning the JSON data of the popup\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;stackoverflow.com\u0026#x2F;a\u0026#x2F;48527419\u0026#x2F;4398037" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;stackoverflow.com\u0026#x2F;a\u0026#x2F;48527419\u0026#x2F;4398037\u003c/a\u003e\u003c/p\u003e\u003cp\u003eDocumentation: \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;api\u0026#x2F;rest_v1\u0026#x2F;#!\u0026#x2F;Page_content\u0026#x2F;get_page_summary_title" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;api\u0026#x2F;rest_v1\u0026#x2F;#!\u0026#x2F;Page_content\u0026#x2F;get_pag...\u003c/a\u003e\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16902012,
      created_at: "2018-04-23T10:07:00.000Z",
      created_at_i: 1524478020,
      type: "comment",
      author: "jopsen",
      title: null,
      url: null,
      text:
        "\u003cp\u003e\u0026gt; We couldn’t expect every single article to be edited to designate a thumbnail.\u003c/p\u003e\u003cp\u003eIt wouldn\u0026#x27;t surprise me if they could. Not to say that automation isn\u0026#x27;t great, and for this purpose probably ideal.\u003c/p\u003e\u003cp\u003eBut, selecting a thumbnail for every Wikipedia article seems like something the community could easily have done.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902883,
          created_at: "2018-04-23T12:45:46.000Z",
          created_at_i: 1524487546,
          type: "comment",
          author: "anc84",
          title: null,
          url: null,
          text:
            "\u003cp\u003eAnd what a completely senseless thing it would be. Reminds me of all the random stockphotos in articles wasting bandwidth and attention for no gain.\u003c/p\u003e",
          points: null,
          parent_id: 16902012,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16909696,
      created_at: "2018-04-24T03:45:43.000Z",
      created_at_i: 1524541543,
      type: "comment",
      author: "cwyers",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThey sunk four years of time into this, with developers, UI people, A\u0026#x2F;B testing... all of it. So they could have thumbnails when you hover over links. And they can\u0026#x27;t pay the people actually writing the encyclopedia.\u003c/p\u003e\u003cp\u003eI hate our current web sometimes. The only skill it seems to know how to reward is writing code. 99% of the value of Wikipedia has nothing to do with code at all. Yet nobody gets rewarded for that.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901768,
      created_at: "2018-04-23T09:09:11.000Z",
      created_at_i: 1524474551,
      type: "comment",
      author: "majewsky",
      title: null,
      url: null,
      text:
        "\u003cp\u003e\u0026gt; People seem to like it — we are seeing 5,000 hits to our API a minute to serve those cards that show when you hover over any link.\u003c/p\u003e\u003cp\u003eUhm, no. It just means that people are hovering over links with their mouse. It does not imply any opinion about the previews.\u003c/p\u003e\u003cp\u003e\u0026gt; The original idea was conceived four years ago\u003c/p\u003e\u003cp\u003eWhen I was active at Wikipedia\u0026#x2F;Wikibooks 12 years ago, there was a user script floating around that did the same thing, except I\u0026#x27;m not sure if it included an image. (Mediawiki allows a user to define custom CSS and JS that get embedded in every page of their user session.)\u003c/p\u003e\u003cp\u003eI don\u0026#x27;t mean to express dislike or downplay the hard work that went into this feature, just to add some context.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902176,
          created_at: "2018-04-23T10:49:49.000Z",
          created_at_i: 1524480589,
          type: "comment",
          author: "Crontab",
          title: null,
          url: null,
          text:
            "\u003cp\u003eI personally do not care for the link preview feature as I am one of those people who like to hover over a link so I can see the url leads (which is also why I detest URL shorteners).\u003c/p\u003e\u003cp\u003eI also have to wonder how much bandwidth Wikipedia is going to end up using to display unneeded previews.\u003c/p\u003e",
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [
            {
              id: 16903864,
              created_at: "2018-04-23T14:51:38.000Z",
              created_at_i: 1524495098,
              type: "comment",
              author: "zhte415",
              title: null,
              url: null,
              text:
                "\u003cp\u003e\u0026gt; I also have to wonder how much bandwidth Wikipedia is going to end up using to display unneeded previews.\u003c/p\u003e\u003cp\u003eThis is interesting.  There\u0026#x27;s an external arrow for external links, so I\u0026#x27;m still good with this use case for inspecting links; for internal links I\u0026#x27;ve found myself hovering where I previously clicked (and loaded) an additional page at least 50% of the time.  Where I\u0026#x27;m OK with a quick paragraph description and can then decide if the linked paragraph requires \u0026#x2F; inspires a deeper read, about 50% of the time.\u003c/p\u003e\u003cp\u003eI think this is a net reduction of bandwidth for my personal use-case.  Wide A\u0026#x2F;B testing would reveal more, obviously.\u003c/p\u003e",
              points: null,
              parent_id: 16902176,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16902600,
              created_at: "2018-04-23T12:00:07.000Z",
              created_at_i: 1524484807,
              type: "comment",
              author: "tuukkah",
              title: null,
              url: null,
              text:
                "\u003cp\u003e\u0026gt; \u003ci\u003eI personally do not care for the link preview feature as I am one of those people who like to hover over a link so I can see the url leads\u003c/i\u003e\u003c/p\u003e\u003cp\u003eBut that\u0026#x27;s exactly what these preview popups are for! A bit heavier than the plain URL, but much more useful and friendlier.\u003c/p\u003e\u003cp\u003e(EDIT: I see there\u0026#x27;s a bug in that it displays the preview even if you don\u0026#x27;t stop the mouse there. Still, this can be \u003ci\u003elighter\u003c/i\u003e than requiring people to click through to the full articles, considering the preview can tell you what you need or that the link is not relevant for you, and slows you going down the rabbit hole to more links. Maybe it should be located at the bottom of the screen like the plain URL popup though?)\u003c/p\u003e",
              points: null,
              parent_id: 16902176,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16901939,
          created_at: "2018-04-23T09:51:21.000Z",
          created_at_i: 1524477081,
          type: "comment",
          author: "KenanSulayman",
          title: null,
          url: null,
          text:
            "\u003cp\u003eAs far as I know the Lupin user-script was the foundation of the PagePreview feature. I guess the Reading group at WikiMedia rewrote the extension as part of MediaWiki core and tried to fix most edge cases on the way.\u003c/p\u003e",
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16906292,
          created_at: "2018-04-23T18:49:57.000Z",
          created_at_i: 1524509397,
          type: "comment",
          author: "jdlrobson",
          title: null,
          url: null,
          text:
            '\u003cp\u003eHey! (author here)\u003c/p\u003e\u003cp\u003eI sincerely regret the use of that statement \u0026quot;people seem to like it\u0026quot; in my post. I\u0026#x27;ve now removed it as I worry this confuses my message so thank you for pointing it out. This really trivializes all the work that went into A\u0026#x2F;B testing this and how we measured success. I really recommend a read of \u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews#Success_Metrics_and_Feature_Evaluation" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews#Success_Metrics...\u003c/a\u003e \nSide note: the volume of traffic was also wrong by several magnitudes... actually 0.5 million)\u003c/p\u003e\u003cp\u003eMy main motivation when writing this post was to share how small changes require magnitudes of effort not to express the merits of the feature. As a developer who works with product owners a lot and often get asked how I can build things quicker (I\u0026#x27;m sure many can relate). I wanted to provide something useful to other developers that easy looking things are not actually easy to build, so thanks for flagging.\u003c/p\u003e\u003cp\u003eWith regards to the user script, yeh that\u0026#x27;s been around for some time and was the seed for this idea. It\u0026#x27;s just taken a long time to get that from such a small audience to a mainstream one. It doesn\u0026#x27;t downplay it in my opinion, just shows how far we\u0026#x27;ve come.\u003c/p\u003e\u003cp\u003eThanks for reading.\u003c/p\u003e',
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16902836,
          created_at: "2018-04-23T12:40:00.000Z",
          created_at_i: 1524487200,
          type: "comment",
          author: "SeanMacConMara",
          title: null,
          url: null,
          text:
            "\u003cp\u003e\u0026gt;Uhm, no. It just means that people are hovering over links with their mouse.\u003c/p\u003e\u003cp\u003ein my case every single time was by accident whilst moving the mouse around, off of other text I was trying to read.\u003c/p\u003e\u003cp\u003efrankly, its bloody annoying\u003c/p\u003e\u003cp\u003eit covered the text i was reading\u003c/p\u003e\u003cp\u003ethe search for yet another \u0026quot;disable\u0026quot; setting begins....\u003c/p\u003e",
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [
            {
              id: 16903711,
              created_at: "2018-04-23T14:35:44.000Z",
              created_at_i: 1524494144,
              type: "comment",
              author: "mygo",
              title: null,
              url: null,
              text:
                "\u003cp\u003e“We thought the floating pop-up billboard showing you a preview of what’s inside when you glance at a building was a great feature as you walk down times square”\u003c/p\u003e\u003cp\u003e— bad UX designers in 2070\u003c/p\u003e",
              points: null,
              parent_id: 16902836,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16903569,
              created_at: "2018-04-23T14:17:45.000Z",
              created_at_i: 1524493065,
              type: "comment",
              author: "jessriedel",
              title: null,
              url: null,
              text:
                '\u003cp\u003e\u0026gt; the search for yet another \u0026quot;disable\u0026quot; setting begins....\u003c/p\u003e\u003cp\u003eYour inability to figure out how to disable it was already counted as success!\u003c/p\u003e\u003cp\u003e\u0026gt; the rates of disabling the feature are negligibly low — a strong indicator that people find it useful.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;medium.com\u0026#x2F;freely-sharing-the-sum-of-all-knowledge\u0026#x2F;wikipedia-page-previews-738cddac7a21" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;medium.com\u0026#x2F;freely-sharing-the-sum-of-all-knowledge\u0026#x2F;w...\u003c/a\u003e\u003c/p\u003e',
              points: null,
              parent_id: 16902836,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16903106,
              created_at: "2018-04-23T13:15:54.000Z",
              created_at_i: 1524489354,
              type: "comment",
              author: "rootlocus",
              title: null,
              url: null,
              text:
                "\u003cp\u003eThe popup has a little gear icon in the bottom right. You can disable it from there.\u003c/p\u003e",
              points: null,
              parent_id: 16902836,
              story_id: 16901599,
              children: [
                {
                  id: 16903238,
                  created_at: "2018-04-23T13:34:17.000Z",
                  created_at_i: 1524490457,
                  type: "comment",
                  author: "freeone3000",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eThey put a small click button on the edge of a hover control? really?\u003c/p\u003e",
                  points: null,
                  parent_id: 16903106,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16905939,
                      created_at: "2018-04-23T18:14:44.000Z",
                      created_at_i: 1524507284,
                      type: "comment",
                      author: "always_good",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eI just tested it and it\u0026#x27;s trivial to click. Are you actually having difficulty or just pretending it\u0026#x27;s hard so you can be condescending?\u003c/p\u003e\u003cp\u003eI think the best place for these controls are usually on the feature itself. I don\u0026#x27;t want to have to search in some independent preferences UI every I want to see if something can be tweaked, but it makes sense to offer it there as well.\u003c/p\u003e",
                      points: null,
                      parent_id: 16903238,
                      story_id: 16901599,
                      children: [],
                      options: []
                    },
                    {
                      id: 16903675,
                      created_at: "2018-04-23T14:31:06.000Z",
                      created_at_i: 1524493866,
                      type: "comment",
                      author: null,
                      title: null,
                      url: null,
                      text: null,
                      points: null,
                      parent_id: 16903238,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                },
                {
                  id: 16903512,
                  created_at: "2018-04-23T14:11:47.000Z",
                  created_at_i: 1524492707,
                  type: "comment",
                  author: "SeanMacConMara",
                  title: null,
                  url: null,
                  text: "\u003cp\u003echeers!\u003c/p\u003e",
                  points: null,
                  parent_id: 16903106,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16902360,
          created_at: "2018-04-23T11:23:51.000Z",
          created_at_i: 1524482631,
          type: "comment",
          author: "subpixel",
          title: null,
          url: null,
          text:
            "\u003cp\u003eI do mean to express dislike, as I\u0026#x27;m one of the millions of users giving Wikipedia a false metric and sense of satisfaction. I\u0026#x27;ve seen the pop-up hundreds of times, and have considered it a hindrance to my process every single time.\u003c/p\u003e\u003cp\u003eSomething that just happens accidentally is a bug, no matter how useful it might be if it were not happening accidentally.\u003c/p\u003e",
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [
            {
              id: 16904432,
              created_at: "2018-04-23T15:46:14.000Z",
              created_at_i: 1524498374,
              type: "comment",
              author: "publicfig",
              title: null,
              url: null,
              text:
                "\u003cp\u003eYou can disable it either from the screen, or if you have an account, permanently from your settings.\u003c/p\u003e",
              points: null,
              parent_id: 16902360,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16901873,
          created_at: "2018-04-23T09:32:53.000Z",
          created_at_i: 1524475973,
          type: "comment",
          author: "panic",
          title: null,
          url: null,
          text:
            "\u003cp\u003e\u003ci\u003e\u0026gt; Uhm, no. It just means that people are hovering over links with their mouse. It does not imply any opinion about the previews.\u003c/i\u003e\u003c/p\u003e\u003cp\u003eWhen you evaluate features using engagement metrics, there are only two possibilities.  If the metric is high, users love the feature.  If the metric is low, users don\u0026#x27;t know the feature exists, and more alerts or \u0026quot;unread\u0026quot; badges must be added to help them learn.\u003c/p\u003e",
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [
            {
              id: 16901894,
              created_at: "2018-04-23T09:39:07.000Z",
              created_at_i: 1524476347,
              type: "comment",
              author: "majewsky",
              title: null,
              url: null,
              text:
                "\u003cp\u003eMy point is that the metric is bad.\u003c/p\u003e",
              points: null,
              parent_id: 16901873,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16901904,
              created_at: "2018-04-23T09:41:17.000Z",
              created_at_i: 1524476477,
              type: "comment",
              author: "mappu",
              title: null,
              url: null,
              text:
                "\u003cp\u003eAlternatively, if it\u0026#x27;s a feature you don\u0026#x27;t like:\u003c/p\u003e\u003cp\u003eIf the metric is high, users are spending too much time on it. Also, it is responsible for every single frustration that users have with your product.\u003c/p\u003e\u003cp\u003eIf the metric is low, the feature can be safely removed (see: Windows Start menu).\u003c/p\u003e",
              points: null,
              parent_id: 16901873,
              story_id: 16901599,
              children: [
                {
                  id: 16902280,
                  created_at: "2018-04-23T11:08:21.000Z",
                  created_at_i: 1524481701,
                  type: "comment",
                  author: "megaman22",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003e\u0026gt; If the metric is low, the feature can be safely removed (see: Windows Start menu).\u003c/p\u003e\u003cp\u003eThat turned out really well...\u003c/p\u003e\u003cp\u003eWhoever is responsible for putting the touch start screen on Windows Server is responsible for 90% of my frustrations...\u003c/p\u003e",
                  points: null,
                  parent_id: 16901904,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16903278,
                      created_at: "2018-04-23T13:39:49.000Z",
                      created_at_i: 1524490789,
                      type: "comment",
                      author: "brazzledazzle",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eThe best way to run Windows Server is without a desktop installed, using PowerShell to manage it. Even one-offs. You might know this but it’s worth saying just in case. Also worth noting that some server applications assume\u0026#x2F;require a desktop during install or operation (usually because they’re trash).\u003c/p\u003e",
                      points: null,
                      parent_id: 16902280,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                },
                {
                  id: 16902599,
                  created_at: "2018-04-23T12:00:06.000Z",
                  created_at_i: 1524484806,
                  type: "comment",
                  author: "chiefalchemist",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eYeah. Time on site, for example, might mean they can\u0026#x27;t find what they need; as opposed to getting what they need and staying for more.\u003c/p\u003e\u003cp\u003eNumbers are helpful, but sometimes they can be misinterpreted to mask the why.\u003c/p\u003e",
                  points: null,
                  parent_id: 16901904,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16901919,
              created_at: "2018-04-23T09:45:29.000Z",
              created_at_i: 1524476729,
              type: "comment",
              author: "dmitriid",
              title: null,
              url: null,
              text:
                "\u003cp\u003e\u0026gt; When you evaluate features using engagement metrics\u003c/p\u003e\u003cp\u003eNo-no-no. Stop right there. \u0026quot;Engagement metrics\u0026quot; are the worst kind of metrics. Engagement means next to nothing. It just means that a user interacted with something. Was it good? Was it bad? Was it intentional? Was it by mistake? \u0026quot;Engagement metrics\u0026quot; answer none of that.\u003c/p\u003e\u003cp\u003eAnd yet... They are the easiest metrics to collect and too many companies use them as if they were meaningful.\u003c/p\u003e",
              points: null,
              parent_id: 16901873,
              story_id: 16901599,
              children: [
                {
                  id: 16903198,
                  created_at: "2018-04-23T13:28:49.000Z",
                  created_at_i: 1524490129,
                  type: "comment",
                  author: "notriddle",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003eI\u0026#x27;m case you were curious, WM has done more user testing than just the engagement metrics:\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;m.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Special:MyLanguage\u0026#x2F;Wikimedia_Research\u0026#x2F;Design_Research\u0026#x2F;Reading_Team_UX_Research\u0026#x2F;Hovercards_Usability" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;m.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Special:MyLanguage\u0026#x2F;Wikimedia_Re...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eNot sure what it says about them that they\u0026#x27;re mentioning worthless numbers instead of the way more useful data they got back during this survey.\u003c/p\u003e',
                  points: null,
                  parent_id: 16901919,
                  story_id: 16901599,
                  children: [],
                  options: []
                },
                {
                  id: 16902982,
                  created_at: "2018-04-23T12:59:20.000Z",
                  created_at_i: 1524488360,
                  type: "comment",
                  author: "mattkrause",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eIf you hadn’t stopped right there, you might have seen the sarcasm in the rest of the post!\u003c/p\u003e",
                  points: null,
                  parent_id: 16901919,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16903218,
              created_at: "2018-04-23T13:32:03.000Z",
              created_at_i: 1524490323,
              type: "comment",
              author: "samjbobb",
              title: null,
              url: null,
              text:
                '\u003cp\u003eLinked from the article is the final A\u0026#x2F;B test they ran: \n\u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews\u0026#x2F;2017-18_A\u0026#x2F;B_Tests" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews\u0026#x2F;2017-18_A\u0026#x2F;B_Tes...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eThey had better metrics than just number of API requests\u003c/p\u003e',
              points: null,
              parent_id: 16901873,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16901991,
              created_at: "2018-04-23T10:02:05.000Z",
              created_at_i: 1524477725,
              type: "comment",
              author: "iofiiiiiiiii",
              title: null,
              url: null,
              text:
                "\u003cp\u003eBut when the feature only needs you to hover over a link to activate (which people do anyway) you have no basis to claim that this is users \u0026quot;liking\u0026quot; the feature - it might just be noise from everyday regular navigation.\u003c/p\u003e",
              points: null,
              parent_id: 16901873,
              story_id: 16901599,
              children: [
                {
                  id: 16902157,
                  created_at: "2018-04-23T10:43:35.000Z",
                  created_at_i: 1524480215,
                  type: "comment",
                  author: "teddyuk",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003ei was using wikipedia to with my son yesterday for a project and found it really annoying, so i\u0026#x27;m in that list of people who hit the api but aren\u0026#x27;t in the list of people who seem to be \u0026quot;liking it\u0026quot;\u003c/p\u003e",
                  points: null,
                  parent_id: 16901991,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16901875,
          created_at: "2018-04-23T09:33:23.000Z",
          created_at_i: 1524476003,
          type: "comment",
          author: "jwilk",
          title: null,
          url: null,
          text:
            '\u003cp\u003eAlso, is 5K hits\u0026#x2F;minute really that impressive?\u003c/p\u003e\u003cp\u003eAccording to \u003ca href="https:\u0026#x2F;\u0026#x2F;stats.wikimedia.org\u0026#x2F;EN\u0026#x2F;Sitemap.htm" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;stats.wikimedia.org\u0026#x2F;EN\u0026#x2F;Sitemap.htm\u003c/a\u003e , English Wikipedia gets 88K views per minute.\u003c/p\u003e',
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [
            {
              id: 16901900,
              created_at: "2018-04-23T09:40:37.000Z",
              created_at_i: 1524476437,
              type: "comment",
              author: "majewsky",
              title: null,
              url: null,
              text:
                "\u003cp\u003eThat was also my first reaction, but I did not mention it because judging this (in relation to pageviews) would require more knowledge about the audience, e.g. how many users are using a mouse at all (vs. a touch screen, or pure keyboard navigation).\u003c/p\u003e",
              points: null,
              parent_id: 16901875,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16903725,
              created_at: "2018-04-23T14:37:23.000Z",
              created_at_i: 1524494243,
              type: "comment",
              author: "mygo",
              title: null,
              url: null,
              text:
                "\u003cp\u003ebut how many of those views are on mobile devices?\u003c/p\u003e\u003cp\u003ethis is a desktop feature. in general, mobile traffic \u0026gt; desktop traffic.\u003c/p\u003e\u003cp\u003ewe’d need wikipedia’s browsing statistics be sure, since some sites see different ratios than others. What if 1:8 wikipedia visits are on desktop? That would make the 5K\u0026#x2F;minute much more significant.\u003c/p\u003e",
              points: null,
              parent_id: 16901875,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16902233,
              created_at: "2018-04-23T10:59:58.000Z",
              created_at_i: 1524481198,
              type: "comment",
              author: "giancarlostoro",
              title: null,
              url: null,
              text:
                "\u003cp\u003eI imagine most people spend more time reading on Wikipedia as opposed to trying to hover over every link available?\u003c/p\u003e",
              points: null,
              parent_id: 16901875,
              story_id: 16901599,
              children: [
                {
                  id: 16902696,
                  created_at: "2018-04-23T12:18:50.000Z",
                  created_at_i: 1524485930,
                  type: "comment",
                  author: "larkeith",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eThat\u0026#x27;s less than one API hit per 17 English pages viewed - the overwhelming majority of the time, users do not use this feature at all, based on these numbers.\u003c/p\u003e\u003cp\u003eThis response is pure sophistry.\u003c/p\u003e",
                  points: null,
                  parent_id: 16902233,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16903919,
                      created_at: "2018-04-23T14:56:24.000Z",
                      created_at_i: 1524495384,
                      type: "comment",
                      author: "kparaju",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eI wouldn\u0026#x27;t be surprised if a lot of the pageviews are bots\u0026#x2F;crawlers. They don\u0026#x27;t hover over links.\u003c/p\u003e",
                      points: null,
                      parent_id: 16902696,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16906429,
              created_at: "2018-04-23T19:07:26.000Z",
              created_at_i: 1524510446,
              type: "comment",
              author: "jdlrobson",
              title: null,
              url: null,
              text:
                "\u003cp\u003eNumber was very very very wrong. Actually it\u0026#x27;s 0.5 million. I\u0026#x27;ve corrected the post.\u003c/p\u003e",
              points: null,
              parent_id: 16901875,
              story_id: 16901599,
              children: [
                {
                  id: 16909134,
                  created_at: "2018-04-24T01:33:19.000Z",
                  created_at_i: 1524533599,
                  type: "comment",
                  author: "tuananh",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eit\u0026#x27;s still shown as 5.5k in grafana. what am i missing? the unit is events per minute.\u003c/p\u003e",
                  points: null,
                  parent_id: 16906429,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16909385,
                      created_at: "2018-04-24T02:24:27.000Z",
                      created_at_i: 1524536667,
                      type: "comment",
                      author: "jdlrobson",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eThe sample size for those graphs is 1% so you need to multiple that by 100. I personally checked the access logs to check they are consistent. That was where the confusion came from!\u003c/p\u003e",
                      points: null,
                      parent_id: 16909134,
                      story_id: 16901599,
                      children: [
                        {
                          id: 16909641,
                          created_at: "2018-04-24T03:28:46.000Z",
                          created_at_i: 1524540526,
                          type: "comment",
                          author: "tuananh",
                          title: null,
                          url: null,
                          text: "\u003cp\u003eAh, thank you.\u003c/p\u003e",
                          points: null,
                          parent_id: 16909385,
                          story_id: 16901599,
                          children: [],
                          options: []
                        }
                      ],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16901887,
              created_at: "2018-04-23T09:38:05.000Z",
              created_at_i: 1524476285,
              type: "comment",
              author: "foota",
              title: null,
              url: null,
              text:
                "\u003cp\u003eI\u0026#x27;m thinking it has to be heavily cached.\u003c/p\u003e",
              points: null,
              parent_id: 16901875,
              story_id: 16901599,
              children: [
                {
                  id: 16902260,
                  created_at: "2018-04-23T11:05:02.000Z",
                  created_at_i: 1524481502,
                  type: "comment",
                  author: "majewsky",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003e\u0026quot;5000 hits to our API per minute\u0026quot; does not say anything about whether they\u0026#x27;re measuring in front of or behind the cache. For all intents and purposes, the cache\u0026#x27;s endpoint \u003ci\u003eis\u003c/i\u003e the API endpoint.\u003c/p\u003e",
                  points: null,
                  parent_id: 16901887,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16902474,
                      created_at: "2018-04-23T11:39:11.000Z",
                      created_at_i: 1524483551,
                      type: "comment",
                      author: "nkozyra",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eAnd uncached hits only would be an even more worthless metric.\u003c/p\u003e\u003cp\u003eI, too, think either that number is wrong or this wasn\u0026#x27;t a feature available to all users.\u003c/p\u003e\u003cp\u003eI also agree that using this metric as affirmation the feature is liked is dangerous; count me among the many here inadvertantly contributing without enjoying the feature.\u003c/p\u003e",
                      points: null,
                      parent_id: 16902260,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16901879,
          created_at: "2018-04-23T09:35:47.000Z",
          created_at_i: 1524476147,
          type: "comment",
          author: "fjarlq",
          title: null,
          url: null,
          text:
            '\u003cp\u003eI expect you\u0026#x27;re remembering User:Lupin\u0026#x2F;popups.js[1], which dates back to August 2005.\u003c/p\u003e\u003cp\u003eIt soon became known as Navigation Popups[2], and is still available today.\u003c/p\u003e\u003cp\u003eThe main documentation for Page Previews mentions Navigation Popups.[3]\u003c/p\u003e\u003cp\u003e[1]: \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;User:Lupin\u0026#x2F;popups.js" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;User:Lupin\u0026#x2F;popups.js\u003c/a\u003e\u003c/p\u003e\u003cp\u003e[2]: \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Tools\u0026#x2F;Navigation_popups" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Tools\u0026#x2F;Navigation_pop...\u003c/a\u003e\u003c/p\u003e\u003cp\u003e[3]: \u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901768,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16901769,
      created_at: "2018-04-23T09:09:56.000Z",
      created_at_i: 1524474596,
      type: "comment",
      author: "shakna",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI am continually impressed by the markup Wikipedia generates.\u003c/p\u003e\u003cp\u003eThey\u0026#x27;ve managed to pull in pretty link previews, scientific notation and a grid layout, whilst building a highly nested markup structure?\u003c/p\u003e\u003cp\u003eThe remarkable part? Wikipedia works great inside a text browser like elinks. It works great in a modern browser. Without sacrificing the interactivity people have grown to expect.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16901809,
          created_at: "2018-04-23T09:18:23.000Z",
          created_at_i: 1524475103,
          type: "comment",
          author: "xab9",
          title: null,
          url: null,
          text:
            "\u003cp\u003eLast time I checked, their markup was pretty much nightmare fuel, but it should work fine with IE5, I\u0026#x27;m sure :)\u003c/p\u003e",
          points: null,
          parent_id: 16901769,
          story_id: 16901599,
          children: [
            {
              id: 16901917,
              created_at: "2018-04-23T09:45:20.000Z",
              created_at_i: 1524476720,
              type: "comment",
              author: "baud147258",
              title: null,
              url: null,
              text:
                "\u003cp\u003eIt works well for the latest (and last) version of IE, at least.\u003c/p\u003e",
              points: null,
              parent_id: 16901809,
              story_id: 16901599,
              children: [
                {
                  id: 16901969,
                  created_at: "2018-04-23T09:58:25.000Z",
                  created_at_i: 1524477505,
                  type: "comment",
                  author: "xab9",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eUsing IE11 with the emulator switched to IE5 I can assure you, it works in IE5. At least the main layout, no popup though :D Still equal parts scary and astonishing.\u003c/p\u003e",
                  points: null,
                  parent_id: 16901917,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16902072,
                      created_at: "2018-04-23T10:19:41.000Z",
                      created_at_i: 1524478781,
                      type: "comment",
                      author: "seba_dos1",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eWikipedia is a content-heavy website first, webapp only second. If it\u0026#x27;s written correctly, then of course it\u0026#x27;s going to work in IE5 - maybe with some parts of the layout looking ugly, maybe with some margins being wrong, some media not being embedded, but generally should be usable.\u003c/p\u003e\u003cp\u003eNowadays it\u0026#x27;s fine for a webdev to not \u003ci\u003etest\u003c/i\u003e their pages on IE5, but when the page is done right, there is no reason for it not to work in IE5, lynx, w3m or Netscape Navigator. Really, it\u0026#x27;s just a webdeveloper\u0026#x27;s job done right.\u003c/p\u003e",
                      points: null,
                      parent_id: 16901969,
                      story_id: 16901599,
                      children: [
                        {
                          id: 16902393,
                          created_at: "2018-04-23T11:28:55.000Z",
                          created_at_i: 1524482935,
                          type: "comment",
                          author: "xab9",
                          title: null,
                          url: null,
                          text:
                            "\u003cp\u003eNope, it\u0026#x27;s not \u0026quot;done right\u0026quot;. It\u0026#x27;s a table layout from the nineties. But probably there\u0026#x27;s an api so noone would have to scrape a wiki page, I never needed to do that fortunately :)\u003c/p\u003e",
                          points: null,
                          parent_id: 16902072,
                          story_id: 16901599,
                          children: [
                            {
                              id: 16911200,
                              created_at: "2018-04-24T09:45:04.000Z",
                              created_at_i: 1524563104,
                              type: "comment",
                              author: "boomlinde",
                              title: null,
                              url: null,
                              text:
                                "\u003cp\u003eJust taking a quick look at the MacOS article posted earlier in this thread, the whole page uses tables exactly as tables should be used: for tables. How do you suppose tables \u003ci\u003eshould\u003c/i\u003e be implemented?\u003c/p\u003e\u003cp\u003eI\u0026#x27;ve seen plenty of idiotic examples of tables being implemented with a bunch of styled \u0026lt;div\u0026gt;, probably because people keep parroting the idea that tables are bad because table based layouts are bad.\u003c/p\u003e",
                              points: null,
                              parent_id: 16902393,
                              story_id: 16901599,
                              children: [],
                              options: []
                            },
                            {
                              id: 16902544,
                              created_at: "2018-04-23T11:50:55.000Z",
                              created_at_i: 1524484255,
                              type: "comment",
                              author: "worble",
                              title: null,
                              url: null,
                              text:
                                "\u003cp\u003eAnd what exactly is not \u0026quot;done right\u0026quot; about that? If it displays the information it\u0026#x27;s supposed to in a clean and organised way, and loads as fast as any other website, then I couldn\u0026#x27;t care less if it\u0026#x27;s a \u0026#x27;90\u0026#x27;s table layout\u0026#x27; or using whatever the hot new JS framework is.\u003c/p\u003e",
                              points: null,
                              parent_id: 16902393,
                              story_id: 16901599,
                              children: [
                                {
                                  id: 16903677,
                                  created_at: "2018-04-23T14:31:26.000Z",
                                  created_at_i: 1524493886,
                                  type: "comment",
                                  author: "Nadya",
                                  title: null,
                                  url: null,
                                  text:
                                    "\u003cp\u003eTry to read today\u0026#x27;s Featured Article in a text-to-speech program. The markup is so mangled it can\u0026#x27;t even read a complete sentence because it breaks anytime the text links out.\u003c/p\u003e",
                                  points: null,
                                  parent_id: 16902544,
                                  story_id: 16901599,
                                  children: [
                                    {
                                      id: 16907555,
                                      created_at: "2018-04-23T21:24:37.000Z",
                                      created_at_i: 1524518677,
                                      type: "comment",
                                      author: "shakna",
                                      title: null,
                                      url: null,
                                      text:
                                        "\u003cp\u003eJAWS didn\u0026#x27;t have any issues for me, which reader are you using?\u003c/p\u003e",
                                      points: null,
                                      parent_id: 16903677,
                                      story_id: 16901599,
                                      children: [
                                        {
                                          id: 16909558,
                                          created_at:
                                            "2018-04-24T03:02:29.000Z",
                                          created_at_i: 1524538949,
                                          type: "comment",
                                          author: "Nadya",
                                          title: null,
                                          url: null,
                                          text:
                                            "\u003cp\u003eNVDA - aka the \u0026quot;2nd choice\u0026quot; - but I think it may have been a configuration issue on my end as the markup on the page, after looking into why NVDA was struggling with it, is just a \u0026lt;p\u0026gt; with some \u0026lt;a\u0026gt; in it which NVDA should handle fine.\u003c/p\u003e",
                                          points: null,
                                          parent_id: 16907555,
                                          story_id: 16901599,
                                          children: [
                                            {
                                              id: 16909876,
                                              created_at:
                                                "2018-04-24T04:39:03.000Z",
                                              created_at_i: 1524544743,
                                              type: "comment",
                                              author: "shakna",
                                              title: null,
                                              url: null,
                                              text:
                                                "\u003cp\u003eNVDA can be difficult to configure, for sure. I couldn\u0026#x27;t get it to play nicely with Sublime, probably my fault. Reason I shelled out for JAWS.\u003c/p\u003e",
                                              points: null,
                                              parent_id: 16909558,
                                              story_id: 16901599,
                                              children: [],
                                              options: []
                                            }
                                          ],
                                          options: []
                                        }
                                      ],
                                      options: []
                                    }
                                  ],
                                  options: []
                                }
                              ],
                              options: []
                            }
                          ],
                          options: []
                        }
                      ],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16901839,
          created_at: "2018-04-23T09:26:48.000Z",
          created_at_i: 1524475608,
          type: "comment",
          author: "saagarjha",
          title: null,
          url: null,
          text:
            '\u003cp\u003eWikipedia\u0026#x27;s markup is just terrible for trying to do any sort of scraping or analysis. I once tried to write a script that pulled the latest version of macOS from the sidebar of this article[1] and I gave up because it was difficult and brittle in a way that made it nearly impossible. I\u0026#x27;d probably have better results parsing the HTML with a regex. Likewise, I know a friend who literally had to scrap an entire project because Wikipedia made it so difficult to get the text of an article without non-word entities in it.\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;MacOS" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;MacOS\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901769,
          story_id: 16901599,
          children: [
            {
              id: 16902165,
              created_at: "2018-04-23T10:45:50.000Z",
              created_at_i: 1524480350,
              type: "comment",
              author: "ryanjshaw",
              title: null,
              url: null,
              text:
                '\u003cp\u003eHow hard did you try?\u003c/p\u003e\u003cp\u003eThese are not hand-written pages, and their output is actually pretty clean compared to the crazy things I\u0026#x27;ve tried to scrape.  They have tons of APIs to access the backing data, and that should be your first stop.\u003c/p\u003e\u003cp\u003eEven if you insist on scraping, in your case you\u0026#x27;re just looking for a \u0026lt;td\u0026gt; whose immediately preceding \u0026lt;td\u0026gt; contains the text \u0026quot;Latest Release\u0026quot;, and that\u0026#x27;s something any XPath-based scraper can give you straight out of the box[1].\u003c/p\u003e\u003cp\u003eA more resilient choice, if you still insist on (or have to use) scraping, is use the underlying template data - a regex is good enough in that case[2]\u003c/p\u003e\u003cp\u003e[1] e.g. \u003ca href="http:\u0026#x2F;\u0026#x2F;html-agility-pack.net\u0026#x2F;" rel="nofollow"\u003ehttp:\u0026#x2F;\u0026#x2F;html-agility-pack.net\u0026#x2F;\u003c/a\u003e\n[2] \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=Template:Latest_stable_software_release\u0026#x2F;macOS\u0026amp;action=edit" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=Template:Latest_s...\u003c/a\u003e\u003c/p\u003e',
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [
                {
                  id: 16904287,
                  created_at: "2018-04-23T15:32:29.000Z",
                  created_at_i: 1524497549,
                  type: "comment",
                  author: "squeaky-clean",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003e\u0026gt; Even if you insist on scraping, in your case you\u0026#x27;re just looking for a \u0026lt;td\u0026gt; whose immediately preceding \u0026lt;td\u0026gt; contains the text \u0026quot;Latest Release\u0026quot;, and that\u0026#x27;s something any XPath-based scraper can give you straight out of the box\u003c/p\u003e\u003cp\u003eSure, until it changes. Here it is in Jan 2016 when it was included in the opening paragraphs as the text \u0026quot;The latest version of OS X is \u0026lt;scrape here\u0026gt;\u0026quot;.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=MacOS\u0026amp;oldid=697692932" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=MacOS\u0026amp;oldid=69769...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eAnd here it is in June 2016 where that sentence was changed to \u0026quot;The latest software version is \u0026lt;scrape here\u0026gt;\u0026quot;\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=MacOS\u0026amp;oldid=725280390" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=MacOS\u0026amp;oldid=72528...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eThen around September 2016 it was moved into the sidebar using the template you linked. It looks like that template has been a consistent and reliable element for this since 2012, but then why was it only used in the OSX infobar in middle\u0026#x2F;late 2016? How else would anyone have known to find it?\u003c/p\u003e\u003cp\u003eAnd this is just OSX, what if OP wanted a web page that tracked the latest stable release for 20 different OS\u0026#x27;s? It ends up being pretty frequent maintenance for a small project.\u003c/p\u003e',
                  points: null,
                  parent_id: 16902165,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16905549,
                      created_at: "2018-04-23T17:31:27.000Z",
                      created_at_i: 1524504687,
                      type: "comment",
                      author: null,
                      title: null,
                      url: null,
                      text: null,
                      points: null,
                      parent_id: 16904287,
                      story_id: 16901599,
                      children: [],
                      options: []
                    },
                    {
                      id: 16907364,
                      created_at: "2018-04-23T21:03:22.000Z",
                      created_at_i: 1524517402,
                      type: "comment",
                      author: "laumars",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eI can\u0026#x27;t help feeling like there is a lot of tool blaming happening when the wrong tools were used in the first place. Wikipedia is pretty easy to scrape general blocks of text (I\u0026#x27;m the author of an IRC bot which did link previewing, inc Wikipedia) but if you need specific, machine readable, passages which aren\u0026#x27;t going to change sentence structure over the years then you really should be getting that information from a proper API which cateloges that information. Even if it means having to buikd your own backend process which polls the websites for the 20 respective OSs individually so you can compile your own API.\u003c/p\u003e\u003cp\u003eUsing an encyclopedia which is constantly being updated and is written to be read by humans as a stable API for machines is just insanity in my honest opinion.\u003c/p\u003e",
                      points: null,
                      parent_id: 16904287,
                      story_id: 16901599,
                      children: [
                        {
                          id: 16909688,
                          created_at: "2018-04-24T03:44:23.000Z",
                          created_at_i: 1524541463,
                          type: "comment",
                          author: "jancsika",
                          title: null,
                          url: null,
                          text:
                            '\u003cp\u003e\u0026gt; I can\u0026#x27;t help feeling like there is a lot of tool blaming happening when the wrong tools were used in the first place.\u003c/p\u003e\u003cp\u003eWell, let\u0026#x27;s be fair: it\u0026#x27;s a bit surprising that a series of clear, readable key\u0026#x2F;value pairs in that Wikipedia \u0026quot;MacOS\u0026quot; infobox table can\u0026#x27;t be delivered by their API as JSON key\u0026#x2F;value pairs.\u003c/p\u003e\u003cp\u003eUsing their API I can generate a JSON that has a big blob sandwiched in there. With the xmlfm format[1] that same blob has some nice-looking \u0026quot;key = value\u0026quot; pairs, too. Funny enough, those pairs for some reason \u003ci\u003eexclude\u003c/i\u003e the \u0026quot;latest release\u0026quot; key.\u003c/p\u003e\u003cp\u003eAnyway, is there any case where a \u0026lt;tr\u0026gt; containing two columns in the Wikipedia infobox table \u003ci\u003edoesn\u0026#x27;t\u003c/i\u003e hold a key\u0026#x2F;value pair? That just seems like such a valuable source of data to make available in simple JSON format.\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisions\u0026amp;rvprop=content\u0026amp;format=xmlfm\u0026amp;titles=MacOS\u0026amp;rvsection=0" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisio...\u003c/a\u003e\u003c/p\u003e',
                          points: null,
                          parent_id: 16907364,
                          story_id: 16901599,
                          children: [],
                          options: []
                        },
                        {
                          id: 16907907,
                          created_at: "2018-04-23T22:09:25.000Z",
                          created_at_i: 1524521365,
                          type: "comment",
                          author: "squeaky-clean",
                          title: null,
                          url: null,
                          text:
                            "\u003cp\u003e\u0026gt; Even if it means having to build your own backend process which polls the websites for the 20 respective OSs individually so you can compile your own API\u003c/p\u003e\u003cp\u003eOne caveat there, a page like that for MacOS doesn\u0026#x27;t exist. Scraping Wikipedia may be insane, but it\u0026#x27;s often the best option. You can scrape macrumors or something, but then you\u0026#x27;re still just parsing a site meant to be read by humans. You also still risk those 20 OS websites changing as much as Wikipedia.\u003c/p\u003e",
                          points: null,
                          parent_id: 16907364,
                          story_id: 16901599,
                          children: [
                            {
                              id: 16910222,
                              created_at: "2018-04-24T05:56:30.000Z",
                              created_at_i: 1524549390,
                              type: "comment",
                              author: "laumars",
                              title: null,
                              url: null,
                              text:
                                "\u003cp\u003eIndeed but I was thinking of endpoints that have remained relatively static because they have been auto generated or a history of scraping. Some Linux distros have pages like that (even if it\u0026#x27;s just a mirror list).\u003c/p\u003e\u003cp\u003eBut my preferred solution would be using whatever endpoint the respective platform uses for notifying their users of updates.\u003c/p\u003e\u003cp\u003eThis strikes me as a solved problem but even if you can\u0026#x27;t find a ready to use API then I\u0026#x27;d probably sign up to a few mailing lists, update my own endpoint manually and offer 3rd party access for a modest subscription.\u003c/p\u003e\u003cp\u003eEither way, scraping an encyclopedia for an English phrase to parse strikes me as the worst of all the possible solutions.\u003c/p\u003e",
                              points: null,
                              parent_id: 16907907,
                              story_id: 16901599,
                              children: [],
                              options: []
                            }
                          ],
                          options: []
                        },
                        {
                          id: 16907977,
                          created_at: "2018-04-23T22:17:22.000Z",
                          created_at_i: 1524521842,
                          type: "comment",
                          author: "rexaliquid",
                          title: null,
                          url: null,
                          text:
                            "\u003cp\u003eAt the very least, parsing the Release History table seems way better than looking for a particular phrase in the text.\u003c/p\u003e",
                          points: null,
                          parent_id: 16907364,
                          story_id: 16901599,
                          children: [],
                          options: []
                        },
                        {
                          id: 16907484,
                          created_at: "2018-04-23T21:15:27.000Z",
                          created_at_i: 1524518127,
                          type: "comment",
                          author: "always_good",
                          title: null,
                          url: null,
                          text:
                            "\u003cp\u003eAgreed. It\u0026#x27;s some mix of the XY problem plus the self-entitlement of \u0026quot;if I had the idea, then it should work.\u0026quot;\u003c/p\u003e\u003cp\u003eYet the classic HNer confuses this for inherent weaknesses in the underlying platform that they then need to share lest someone has something good to say about the platform. And they\u0026#x27;ll often be using words like \u0026quot;terrible\u0026quot;, \u0026quot;garbage\u0026quot;, and \u0026quot;I hate...\u0026quot;\u003c/p\u003e",
                          points: null,
                          parent_id: 16907364,
                          story_id: 16901599,
                          children: [
                            {
                              id: 16907943,
                              created_at: "2018-04-23T22:13:16.000Z",
                              created_at_i: 1524521596,
                              type: "comment",
                              author: "squeaky-clean",
                              title: null,
                              url: null,
                              text:
                                "\u003cp\u003ePlease, no one said Wikipedia was terrible. You\u0026#x27;re taking statements out of context. The original comment said:\u003c/p\u003e\u003cp\u003e\u0026gt; Wikipedia\u0026#x27;s markup is just terrible for trying to do any sort of scraping or analysis.\u003c/p\u003e\u003cp\u003eI\u0026#x27;d like to emphasize the \u0026quot;for trying to do any sort of scraping or analysis.\u0026quot; Should we instead lie and say it\u0026#x27;s wonderful for scraping?\u003c/p\u003e\u003cp\u003eIt\u0026#x27;s not an insult, it\u0026#x27;s the truth. If you want to build an app that automatically parses Wikipedia, it will not be easy.\u003c/p\u003e",
                              points: null,
                              parent_id: 16907484,
                              story_id: 16901599,
                              children: [
                                {
                                  id: 16910148,
                                  created_at: "2018-04-24T05:43:22.000Z",
                                  created_at_i: 1524548602,
                                  type: "comment",
                                  author: "laumars",
                                  title: null,
                                  url: null,
                                  text:
                                    "\u003cp\u003eBut again, that\u0026#x27;s the wrong tool for the job so of course it\u0026#x27;s not going to be well suited. When it\u0026#x27;s that obvious of a wrong tool saying it\u0026#x27;s terrible is still kind of silly. It\u0026#x27;s like saying hammers are terrible at screwing things or cars make terrible trampolines.\u003c/p\u003e",
                                  points: null,
                                  parent_id: 16907943,
                                  story_id: 16901599,
                                  children: [],
                                  options: []
                                }
                              ],
                              options: []
                            }
                          ],
                          options: []
                        }
                      ],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16902467,
              created_at: "2018-04-23T11:38:32.000Z",
              created_at_i: 1524483512,
              type: "comment",
              author: "maxerickson",
              title: null,
              url: null,
              text:
                '\u003cp\u003eThe grand vision seems to be that you would retrieve it from Wikidata (\u003ca href="https:\u0026#x2F;\u0026#x2F;www.wikidata.org\u0026#x2F;wiki\u0026#x2F;Q14116" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.wikidata.org\u0026#x2F;wiki\u0026#x2F;Q14116\u003c/a\u003e ).\u003c/p\u003e\u003cp\u003eOf course that is out of date and not in sync with the Wikipedia article. But there\u0026#x27;s public query services you can use to fetch stuff from there, you wouldn\u0026#x27;t need to parse html.\u003c/p\u003e',
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16901965,
              created_at: "2018-04-23T09:57:34.000Z",
              created_at_i: 1524477454,
              type: "comment",
              author: "TuringTest",
              title: null,
              url: null,
              text:
                '\u003cp\u003eHave you tried using the mediawiki API [1], or any of the alternatives?[2]\u003c/p\u003e\u003cp\u003eI don\u0026#x27;t know how well they work, but the built-in parser should give you the text without markup. And since they switched to Parsoid [3] to support the Visual Editor, the\u0026#x27;ve polished the wikitext formal specification so all instances of markup have a well-defined structure.\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;API:Parsing_wikitext" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;API:Parsing_wikitext\u003c/a\u003e\u003c/p\u003e\u003cp\u003e[2] \u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Alternative_parsers" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Alternative_parsers\u003c/a\u003e\u003c/p\u003e\u003cp\u003e[3] \u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Parsoid" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Parsoid\u003c/a\u003e\u003c/p\u003e',
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [
                {
                  id: 16907119,
                  created_at: "2018-04-23T20:32:24.000Z",
                  created_at_i: 1524515544,
                  type: "comment",
                  author: "rspeer",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eI\u0026#x27;ve also had to parse Wikitext. The fact that there are 54 parsers in various states of disrepair listed here (and I have written a 55th) is not because people really like reinventing this wheel; it\u0026#x27;s because the complete task is absolutely insurmountable, and everyone needs a different piece of it solved.\u003c/p\u003e\u003cp\u003eThe moment a template gets involved, the structure of an article is \u003ci\u003enot\u003c/i\u003e well-defined. Templates can call MediaWiki built-ins that are implemented in PHP, or extensions that are implemented in Lua. Templates can output more syntax that depends on the surrounding context, kind of like unsafe macros in C. Error-handling is ad-hoc and certain pages depend on the undefined results of error handling. The end result is only defined by the exact pile of code that the site is running.\u003c/p\u003e\u003cp\u003eIf you reproduce that exact pile of code... now you can parse Wikitext into HTML that looks like Wikipedia. That\u0026#x27;s probably not what you needed, and if it was, you could have used a web scraping library.\u003c/p\u003e\u003cp\u003eIt\u0026#x27;s a mess and Visual Editor has not cleaned it up. The problem is that the syntax of Wikitext wasn\u0026#x27;t designed; like everything else surrounding Wikipedia, it happened by vague consensus.\u003c/p\u003e",
                  points: null,
                  parent_id: 16901965,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16909684,
              created_at: "2018-04-24T03:43:25.000Z",
              created_at_i: 1524541405,
              type: "comment",
              author: "tim333",
              title: null,
              url: null,
              text:
                "\u003cp\u003eI think some of your issues are just inherent to the fact it\u0026#x27;s a wiki rather than the design of the markup. I mean I could edit the page just now from \u0026quot;Latest release\u0026quot; to \u0026quot;Latest version\u0026quot; or some such - it\u0026#x27;s just how wikis are.\u003c/p\u003e",
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16903552,
              created_at: "2018-04-23T14:15:48.000Z",
              created_at_i: 1524492948,
              type: "comment",
              author: null,
              title: null,
              url: null,
              text: null,
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16902064,
              created_at: "2018-04-23T10:17:45.000Z",
              created_at_i: 1524478665,
              type: "comment",
              author: "StrangeSound",
              title: null,
              url: null,
              text:
                '\u003cp\u003eCan\u0026#x27;t you use their API? \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;api\u0026#x2F;rest_v1\u0026#x2F;#!\u0026#x2F;Page_content\u0026#x2F;get_page" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;api\u0026#x2F;rest_v1\u0026#x2F;#!\u0026#x2F;Page_content\u0026#x2F;get_pag...\u003c/a\u003e\u003c/p\u003e',
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [
                {
                  id: 16904378,
                  created_at: "2018-04-23T15:41:44.000Z",
                  created_at_i: 1524498104,
                  type: "comment",
                  author: "squeaky-clean",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003eI haven\u0026#x27;t used their API in a long time, but I don\u0026#x27;t think there is a reliable way to get the sidebar that isn\u0026#x27;t worse than HTML.\u003c/p\u003e\u003cp\u003eFor example \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisions\u0026amp;rvprop=content\u0026amp;format=xml\u0026amp;titles=Hacker+News\u0026amp;rvsection=0" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisio...\u003c/a\u003e\u003c/p\u003e\u003cp\u003ereturns\u003c/p\u003e\u003cp\u003e\u003cpre\u003e\u003ccode\u003e    {{Infobox website | name = Hacker News | logo = File:hackernews_logo.png | logo_size = 100px | type = [[News aggregator]] | url = {{url|https:\u0026#x2F;\u0026#x2F;news.ycombinator.com\u0026#x2F;}} | screenshot = File:hn_screenshot.png | registration = Optional | programming_language = [[Arc (programming language)|Arc]] | founder = [[Paul Graham (computer programmer)|Paul Graham]] | launch date = {{start date and age|2007|02|19}} | current status = Online | owner = [[Y Combinator (company)|Y Combinator]] | language = [[English language|English]] }} \u0026#x27;\u0026#x27;\u0026#x27;Hacker News\u0026#x27;\u0026#x27;\u0026#x27; is a [[social news]] website focusing on [[Computer Science|computer science]] and [[Startup company|entrepreneurship]]. It is run by [[Paul Graham (computer programmer)|Paul Graham]]\u0026#x27;s investment fund and startup incubator, [[Y Combinator (company)|Y Combinator]]. In general, content that can be submitted is defined as \u0026quot;anything that gratifies one\u0026#x27;s intellectual curiosity\u0026quot;.\u0026lt;ref\u0026gt;{{cite news | first = Paul | last = Graham | title = Hacker News Guidelines | url = http:\u0026#x2F;\u0026#x2F;ycombinator.com\u0026#x2F;newsguidelines.html | accessdate = 2009-04-29 }}\u0026lt;\u0026#x2F;ref\u0026gt;\n\u003c/code\u003e\u003c/pre\u003e\nWhich isn\u0026#x27;t very easy to parse either. From a cursory search, a better format doesn\u0026#x27;t seem possible without using a 3rd party like dbpedia.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;www.quora.com\u0026#x2F;Does-the-Wikipedia-API-give-structured-information-on-the-infobox" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.quora.com\u0026#x2F;Does-the-Wikipedia-API-give-structured...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eI tried it for OSX, and it actually just returns a redirect statement to MacOS. So expect your tool consuming the API to break if you don\u0026#x27;t handle that in advance.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisions\u0026amp;rvprop=content\u0026amp;format=xml\u0026amp;titles=OSX\u0026amp;rvsection=0" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisio...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eAnd then when trying it for MacOS, I can\u0026#x27;t actually find the version info anywhere in the response data. So you couldn\u0026#x27;t even get that data without scraping the page.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisions\u0026amp;rvprop=content\u0026amp;format=xml\u0026amp;titles=MacOS\u0026amp;rvsection=0" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;w\u0026#x2F;api.php?action=query\u0026amp;prop=revisio...\u003c/a\u003e\u003c/p\u003e',
                  points: null,
                  parent_id: 16902064,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16901883,
              created_at: "2018-04-23T09:36:47.000Z",
              created_at_i: 1524476207,
              type: "comment",
              author: "shakna",
              title: null,
              url: null,
              text:
                "\u003cp\u003eOh, I know. The markup is incomprehensible, but not to a render engine. It doesn\u0026#x27;t even seem to impact loading speed. It generates amazing machine-text.\u003c/p\u003e\u003cp\u003eAs for scraping... Parsing the hell that is wikitext is all you can do. Or apparently, pipe it through a text browser.\u003c/p\u003e",
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [
                {
                  id: 16901912,
                  created_at: "2018-04-23T09:44:17.000Z",
                  created_at_i: 1524476657,
                  type: "comment",
                  author: "saagarjha",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003e\u0026gt; pipe it through a text browser\u003c/p\u003e\u003cp\u003eThat\u0026#x27;s an interesting idea, and one that I hadn\u0026#x27;t thought of, but I\u0026#x27;d place it closer to matching HTML with regex than actual \u0026quot;parsing\u0026quot;. I might use it if I\u0026#x27;m really desperate though.\u003c/p\u003e",
                  points: null,
                  parent_id: 16901883,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16902141,
              created_at: "2018-04-23T10:39:59.000Z",
              created_at_i: 1524479999,
              type: "comment",
              author: "Vinnl",
              title: null,
              url: null,
              text:
                '\u003cp\u003eI hit the same thing recently, but that\u0026#x27;s basically what Wikidata was founded for - and I\u0026#x27;m sure it has the latest version of macOS. It\u0026#x27;s really easy to fetch Wikidata data using the Wikidata API (my example: \u003ca href="https:\u0026#x2F;\u0026#x2F;gitlab.com\u0026#x2F;Flockademic\u0026#x2F;whereisscihub\u0026#x2F;blob\u0026#x2F;master\u0026#x2F;index.js#L122" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;gitlab.com\u0026#x2F;Flockademic\u0026#x2F;whereisscihub\u0026#x2F;blob\u0026#x2F;master\u0026#x2F;ind...\u003c/a\u003e )\u003c/p\u003e',
              points: null,
              parent_id: 16901839,
              story_id: 16901599,
              children: [
                {
                  id: 16907036,
                  created_at: "2018-04-23T20:21:41.000Z",
                  created_at_i: 1524514901,
                  type: "comment",
                  author: "saagarjha",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eUnfortunately that data isn\u0026#x27;t granular enough for what I need: I\u0026#x27;m looking for the build number, which Wikipedia somehow keeps up to date.\u003c/p\u003e",
                  points: null,
                  parent_id: 16902141,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16908421,
                      created_at: "2018-04-23T23:31:41.000Z",
                      created_at_i: 1524526301,
                      type: "comment",
                      author: "Someone",
                      title: null,
                      url: null,
                      text:
                        '\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;MacOS" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;MacOS\u003c/a\u003e has a footnote after the build number. That led me to \u003ca href="https:\u0026#x2F;\u0026#x2F;developer.apple.com\u0026#x2F;news\u0026#x2F;releases\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;developer.apple.com\u0026#x2F;news\u0026#x2F;releases\u0026#x2F;\u003c/a\u003e. I guess that doesn’t do semantic markup, and I didn’t look at the html at all, but it looks like it could provide you what you want fairly easily (likely not 100% reliably if automated, but chances are Wikipedia‘s \u003ci\u003esomehow keeps up to date\u003c/i\u003e involves humans, too)\u003c/p\u003e\u003cp\u003eAlternatively, buy a Mac, set it to auto-update, have cron or launchd reboot it reboot it twice a day, and read the version info from the CLI after rebooting (\u003ca href="https:\u0026#x2F;\u0026#x2F;coderwall.com\u0026#x2F;p\u0026#x2F;4yz8dq\u0026#x2F;determine-os-x-version-from-the-command-line" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;coderwall.com\u0026#x2F;p\u0026#x2F;4yz8dq\u0026#x2F;determine-os-x-version-from-t...\u003c/a\u003e)\u003c/p\u003e',
                      points: null,
                      parent_id: 16907036,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                },
                {
                  id: 16902312,
                  created_at: "2018-04-23T11:14:55.000Z",
                  created_at_i: 1524482095,
                  type: "comment",
                  author: "mmarx",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003eIf you\u0026#x27;re just interested in a single value, using the SPARQL endpoint[0] is probably still more simple, since you don\u0026#x27;t have to filter out deprecated statements, for example.\u003c/p\u003e\u003cp\u003e[0] \u003ca href="http:\u0026#x2F;\u0026#x2F;tinyurl.com\u0026#x2F;ya957wem" rel="nofollow"\u003ehttp:\u0026#x2F;\u0026#x2F;tinyurl.com\u0026#x2F;ya957wem\u003c/a\u003e\u003c/p\u003e',
                  points: null,
                  parent_id: 16902141,
                  story_id: 16901599,
                  children: [],
                  options: []
                },
                {
                  id: 16906458,
                  created_at: "2018-04-23T19:12:07.000Z",
                  created_at_i: 1524510727,
                  type: "comment",
                  author: "IAmEveryone",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eI have a lot of goodwill toward wikimedia, but trying to use wiki data made me question my life choices. It doesn’t help that the official API endpoint Times out for anything mildly complicated. (As in a simple aggravation or sorting in the query)\u003c/p\u003e",
                  points: null,
                  parent_id: 16902141,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16906456,
          created_at: "2018-04-23T19:11:01.000Z",
          created_at_i: 1524510661,
          type: "comment",
          author: "fwdpropaganda",
          title: null,
          url: null,
          text:
            "\u003cp\u003eTo me the remarkable part is that Wikipeda stills works great without javascript.\u003c/p\u003e",
          points: null,
          parent_id: 16901769,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16904075,
      created_at: "2018-04-23T15:11:36.000Z",
      created_at_i: 1524496296,
      type: "comment",
      author: "aerovistae",
      title: null,
      url: null,
      text:
        "\u003cp\u003eHow do you turn this feature on? I don\u0026#x27;t have it.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16904117,
          created_at: "2018-04-23T15:15:21.000Z",
          created_at_i: 1524496521,
          type: "comment",
          author: "F_r_k",
          title: null,
          url: null,
          text:
            "\u003cp\u003eEnable js ?, Or if you are logged, must turn it on\u003c/p\u003e",
          points: null,
          parent_id: 16904075,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16904589,
      created_at: "2018-04-23T16:02:37.000Z",
      created_at_i: 1524499357,
      type: "comment",
      author: "relics443",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI discovered the feature by mistake, and I thought it was cool. I usually browse Wikipedia on my phone, so I don\u0026#x27;t know if I\u0026#x27;d still think it was cool if there are constant false positives on it.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16905876,
      created_at: "2018-04-23T18:08:02.000Z",
      created_at_i: 1524506882,
      type: "comment",
      author: "anotherfounder",
      title: null,
      url: null,
      text:
        '\u003cp\u003eI feel like this is some dystopian alternate reality post. It took 4 years to release a hover popup! Take that in for a second. And the post seems extremely proud, and self-congratulatory about it.\u003c/p\u003e\u003cp\u003eFrom the post: \n\u0026gt; Our initial version wasn’t good enough. Our community asked us not to go ahead with it. We answered by listening to them and making it better.\u003c/p\u003e\u003cp\u003eThis was 2 years ago, and read the comments on the 39 votes it took to not release Hovercards - \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Village_pump_%28proposals%29\u0026#x2F;Archive_131#Proposal:_Enable_Hovercards_by_default" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Village_pump_%28prop...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eThis meant that the silent majority didn\u0026#x27;t get these features for 2 years because some considered it \u0026#x27;intrusive\u0026#x27;. A feature that you could objectively argue as being a useful utility.\u003c/p\u003e\u003cp\u003eIf anything, I think this is an indictment of the complaints against Wikipedia from those observing it and ex-employees. While a benevolent dictatorship might be going too far, such a community process where only the loudest voice wins (over considering what is best for MOST of the users), is surely a broken process. I am surprised that product \u0026amp; design people work there at all in such an environment.\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16906339,
          created_at: "2018-04-23T18:55:30.000Z",
          created_at_i: 1524509730,
          type: "comment",
          author: "ckoerner",
          title: null,
          url: null,
          text:
            '\u003cp\u003e\u0026gt;And the post seems extremely proud, and self-congratulatory about it.\u003c/p\u003e\u003cp\u003eWell, yeah. Doing anything successfully at the scale of Wikipedia is worthy of some praise - and I say that as a US midwesterner - a culture not exactly known as the epitome of hubris. :)\u003c/p\u003e\u003cp\u003eYou might claim I have Stockholm syndrome or something since I worked with the team that developed this feature, but the discussion you highlight did have valid feedback. The process for respecting community governance and developing consensus is more complicated than any one person could imagine. It is frustrating and imperfect. Folks at the foundation like myself are trying to do better in how we approach, work with, and deploy software changes. I agree too that it took a long time to develop, but that\u0026#x27;s not on any one single community\u0026#x27;s shoulders.\u003c/p\u003e\u003cp\u003eFor instance, after doing due diligence we approached the English community again earlier this month and the result of that discussion was quite boring.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Village_pump_(miscellaneous)#Page_Previews" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Village_pump_(miscel...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eFor a technical example that lead to the time it took, the team looked at how we were generating the previews and saw an opportunity to improve them. Previously we tried to parse a bunch of wikitext with a list as long as my arm of exclusions and edge cases. Then the team figured out a way to return HTML summaries from the source article. Not just something useful for this feature and a huge improvement to how information is rendered (like math formulas). Refactoring the code and implementing a new API endpoint took time.\u003c/p\u003e\u003cp\u003eI hope this doesn\u0026#x27;t come across as too argumentative, I wanted to provide an alternative perspective from someone who works daily with product teams and communities within the Wikimedia movement.\u003c/p\u003e',
          points: null,
          parent_id: 16905876,
          story_id: 16901599,
          children: [
            {
              id: 16908336,
              created_at: "2018-04-23T23:16:32.000Z",
              created_at_i: 1524525392,
              type: "comment",
              author: "anotherfounder",
              title: null,
              url: null,
              text:
                "\u003cp\u003eThanks for engaging! To clarify my point, I do think that a the process was followed, and it did lead to some good points but if a process is taking 4+ years to launch something relatively \u003ci\u003esimple\u003c/i\u003e (compared to what other companies with similar scale and teams might launch), then the process itself is flawed. I\u0026#x27;m respectful of your work, but critical of the system that it operates under.\u003c/p\u003e\u003cp\u003eOne could argue that Wikipedia has a broader responsibility to the readers than to just the editors, and such a process gives the editors undue weight in the process. The vocal minority cannot always represent the needs of the silent majority and that role would lie with the product team, which in my understanding hasn\u0026#x27;t been the case at Wikipedia (I say this, and having interviewed and turned down a Wikipedia PM offer and having a few friends worked in Design at Wikipedia and leaving disillusioned).\u003c/p\u003e",
              points: null,
              parent_id: 16906339,
              story_id: 16901599,
              children: [
                {
                  id: 16908540,
                  created_at: "2018-04-23T23:51:59.000Z",
                  created_at_i: 1524527519,
                  type: "comment",
                  author: "scrollaway",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eWikipedia can exist without readers, but not without editors.\u003c/p\u003e",
                  points: null,
                  parent_id: 16908336,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16909766,
          created_at: "2018-04-24T04:04:58.000Z",
          created_at_i: 1524542698,
          type: "comment",
          author: "tim333",
          title: null,
          url: null,
          text:
            '\u003cp\u003eWell 4 years to officially release. It\u0026#x27;s been available as a beta since 2014 and I\u0026#x27;ve been using it for ages \u003ca href="https:\u0026#x2F;\u0026#x2F;lifehacker.com\u0026#x2F;the-best-wikipedia-features-still-in-beta-1654944810" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;lifehacker.com\u0026#x2F;the-best-wikipedia-features-still-in-...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16905876,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16901783,
      created_at: "2018-04-23T09:14:01.000Z",
      created_at_i: 1524474841,
      type: "comment",
      author: "fapjacks",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThat preview window has been a lifesaver. Honestly, as someone with ADD (or ADHD or whatever it\u0026#x27;s called these days), Wikipedia is a fucking minefield. I regularly have many Wikipedia articles open in sometimes ten or twenty different windows, each with anywhere from twenty to fifty tabs (I open a new window to delineate a completely new tangent, or if opening a new tab will cause the tab icons to disappear, otherwise I open a new tab in the current window -- I like tabs and make heavy use of Session Buddy and The Great Suspender). The preview window has really cut a lot of the extraneous BS out of my evening-destroying rabbit holes. I figured it was probably a difficult thing to develop, but if anyone involved with its creation passes by this lowly post, please know that you have my sincerest gratitude for making such a meaningful, useful tool.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902965,
          created_at: "2018-04-23T12:57:05.000Z",
          created_at_i: 1524488225,
          type: "comment",
          author: null,
          title: null,
          url: null,
          text: null,
          points: null,
          parent_id: 16901783,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16902454,
          created_at: "2018-04-23T11:36:44.000Z",
          created_at_i: 1524483404,
          type: "comment",
          author: "azeirah",
          title: null,
          url: null,
          text:
            '\u003cp\u003eWikipedia\u0026#x27;s a minefield for people with ADD, yep.\u003c/p\u003e\u003cp\u003eBut oddly enough, I found that Stackoverflow\u0026#x27;s \u0026quot;hot questions\u0026quot; or whatever thing is equally distracting.\u003c/p\u003e\u003cp\u003eI\u0026#x27;m on a professional software developer network, and then I see a super interesting and legitimate question aaaaaannnddd... I\u0026#x27;m in a rabbithole.\u003c/p\u003e\u003cp\u003eDon\u0026#x27;t know if it\u0026#x27;s helpful or not for you, but I found an extension that limits the maximum amount of tabs you can have open at a given moment, I set it to 3-4, it forces me to decide what link I do or do not want to click on. Makes me conscious of my unconscious browsing habits, might be useful for you too: \u003ca href="https:\u0026#x2F;\u0026#x2F;addons.mozilla.org\u0026#x2F;en-US\u0026#x2F;firefox\u0026#x2F;addon\u0026#x2F;max-tabs-web-ext\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;addons.mozilla.org\u0026#x2F;en-US\u0026#x2F;firefox\u0026#x2F;addon\u0026#x2F;max-tabs-web-...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901783,
          story_id: 16901599,
          children: [
            {
              id: 16903723,
              created_at: "2018-04-23T14:37:12.000Z",
              created_at_i: 1524494232,
              type: "comment",
              author: "kreetx",
              title: null,
              url: null,
              text:
                "\u003cp\u003eClearly you have too much free time at work ;-)\u003c/p\u003e\u003cp\u003eJust kidding,  know how those rabbitholes are.\u003c/p\u003e",
              points: null,
              parent_id: 16902454,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16902757,
              created_at: "2018-04-23T12:28:52.000Z",
              created_at_i: 1524486532,
              type: "comment",
              author: "chatmasta",
              title: null,
              url: null,
              text:
                "\u003cp\u003eBeen there. That\u0026#x27;s why I rarely comment on StackOverflow; it could easily become an addiction, and I can\u0026#x27;t exactly add it to \u0026#x2F;etc\u0026#x2F;hosts like I can other distractions!\u003c/p\u003e",
              points: null,
              parent_id: 16902454,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16902610,
              created_at: "2018-04-23T12:01:42.000Z",
              created_at_i: 1524484902,
              type: "comment",
              author: "fapjacks",
              title: null,
              url: null,
              text:
                "\u003cp\u003eOh, yes, SO\u0026#x27;s hot network questions is a hardcore distraction for me, too. Even for things I really don\u0026#x27;t care about! Like all those workplace drama questions (do so many people \u003ci\u003ereally\u003c/i\u003e think going to HR is going to do them any good at all?). The extension you linked is a very interesting concept, something I hadn\u0026#x27;t thought about using before. But I like the idea of paying a little bit of thought up front to help keep things from exploding down the line. Thanks!\u003c/p\u003e",
              points: null,
              parent_id: 16902454,
              story_id: 16901599,
              children: [
                {
                  id: 16903698,
                  created_at: "2018-04-23T14:33:35.000Z",
                  created_at_i: 1524494015,
                  type: "comment",
                  author: "baud147258",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eSO\u0026#x27;s hot network questions was so bad that I enabled my ad-blocker on SO and added a custom rule to hide that part of the page. At least now when I go to find an answer on SO without falling in a 30-60 min rabbit hole.\u003c/p\u003e",
                  points: null,
                  parent_id: 16902610,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16903991,
                      created_at: "2018-04-23T15:03:24.000Z",
                      created_at_i: 1524495804,
                      type: "comment",
                      author: "pimlottc",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003ePlease post this rule, it would be very helpful.\u003c/p\u003e\u003cp\u003eWhat’s frustrating is that this has been brought up as an issue on StackExchange and rejected as WONTFIX.\u003c/p\u003e",
                      points: null,
                      parent_id: 16903698,
                      story_id: 16901599,
                      children: [
                        {
                          id: 16904218,
                          created_at: "2018-04-23T15:25:34.000Z",
                          created_at_i: 1524497134,
                          type: "comment",
                          author: "baud147258",
                          title: null,
                          url: null,
                          text:
                            '\u003cp\u003eI\u0026#x27;m using uBlock origin. The rule is:\u003c/p\u003e\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  stackoverflow.com###hot-network-questions\n\u003c/code\u003e\u003c/pre\u003e\nI\u0026#x27;ve used the selector tool of uBlock to do it.\u003c/p\u003e\u003cp\u003eEdit: more on this: \u003ca href="https:\u0026#x2F;\u0026#x2F;meta.stackexchange.com\u0026#x2F;questions\u0026#x2F;222721" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;meta.stackexchange.com\u0026#x2F;questions\u0026#x2F;222721\u003c/a\u003e. Linked in one of the answer is an extension: \u003ca href="https:\u0026#x2F;\u0026#x2F;chrome.google.com\u0026#x2F;webstore\u0026#x2F;detail\u0026#x2F;sidebaroverflow\u0026#x2F;lhieihmjhlbhpjkamdjfjldcapnmhddp?hl=en" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;chrome.google.com\u0026#x2F;webstore\u0026#x2F;detail\u0026#x2F;sidebaroverflow\u0026#x2F;lh...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eThere are also other solutions on that page.\u003c/p\u003e',
                          points: null,
                          parent_id: 16903991,
                          story_id: 16901599,
                          children: [],
                          options: []
                        },
                        {
                          id: 16904223,
                          created_at: "2018-04-23T15:25:46.000Z",
                          created_at_i: 1524497146,
                          type: "comment",
                          author: "Roujo",
                          title: null,
                          url: null,
                          text:
                            "\u003cp\u003eThese rules work for me, using uBlock Origin:\u003c/p\u003e\u003cp\u003e\u003cpre\u003e\u003ccode\u003e    stackoverflow.com###hot-network-questions\n    stackoverflow.com###feed-link\u003c/code\u003e\u003c/pre\u003e\u003c/p\u003e",
                          points: null,
                          parent_id: 16903991,
                          story_id: 16901599,
                          children: [],
                          options: []
                        }
                      ],
                      options: []
                    }
                  ],
                  options: []
                },
                {
                  id: 16903799,
                  created_at: "2018-04-23T14:44:36.000Z",
                  created_at_i: 1524494676,
                  type: "comment",
                  author: "icc97",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003e\u0026gt; Like all those workplace drama questions\u003c/p\u003e\u003cp\u003eThey\u0026#x27;re like a soap opera for geeks\u003c/p\u003e",
                  points: null,
                  parent_id: 16902610,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16909814,
                      created_at: "2018-04-24T04:18:37.000Z",
                      created_at_i: 1524543517,
                      type: "comment",
                      author: "failrate",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eOmg, do you remember the milk boiled in electric kettle incident? That\u0026#x27;s the only time I\u0026#x27;ve seen something get posted to Workplace and then have follow up questions go to (iirc) Interpersonal Skills and then Physics.\u003c/p\u003e",
                      points: null,
                      parent_id: 16903799,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                },
                {
                  id: 16903131,
                  created_at: "2018-04-23T13:19:09.000Z",
                  created_at_i: 1524489549,
                  type: "comment",
                  author: "jgtrosh",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003eThe extremist view in this sense is that tabs in general are a bad model for navigation. If all the pages you choose to visit are first-level citizens in your environment (windows) then you think about them more actively. A browser like \u003ca href="https:\u0026#x2F;\u0026#x2F;surf.suckless.org" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;surf.suckless.org\u003c/a\u003e simplifies the browser abstraction well in these regards. I have found it is nearly impossible to get common browsers to work without tabs. This kind of endeavour probably begets a correct window manager like \u003ca href="https:\u0026#x2F;\u0026#x2F;i3wm.org" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;i3wm.org\u003c/a\u003e\u003c/p\u003e',
                  points: null,
                  parent_id: 16902610,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16910612,
                      created_at: "2018-04-24T07:26:00.000Z",
                      created_at_i: 1524554760,
                      type: "comment",
                      author: "kqr",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eI strongly agree. I had to stop using Surf because of its ancient rendering engine which butchers a lot of the sensible parts of the modern web, but I do miss the tabless part of it.\u003c/p\u003e\u003cp\u003eEdit: Also because it does not have Pentadactyl. My dream browser would be tabless and with sensible and powerful Vim bindings. Ideally built on an engine of Common Lisp too...\u003c/p\u003e",
                      points: null,
                      parent_id: 16903131,
                      story_id: 16901599,
                      children: [
                        {
                          id: 16911476,
                          created_at: "2018-04-24T10:58:57.000Z",
                          created_at_i: 1524567537,
                          type: "comment",
                          author: "jgtrosh",
                          title: null,
                          url: null,
                          text:
                            "\u003cp\u003eHave you looked at luakit? It\u0026#x27;s also based on webkit2gtk but it\u0026#x27;s extensible with Lua; I love the simplicity of suckless tools but when a functionality that seems good must be patched in it rarely ends well.\u003c/p\u003e\u003cp\u003eAlso regarding Vi keybindings check out vimb maybe?\u003c/p\u003e",
                          points: null,
                          parent_id: 16910612,
                          story_id: 16901599,
                          children: [],
                          options: []
                        }
                      ],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16905562,
          created_at: "2018-04-23T17:34:01.000Z",
          created_at_i: 1524504841,
          type: "comment",
          author: "jdlrobson",
          title: null,
          url: null,
          text:
            "\u003cp\u003e\u0026lt;3 message received from \u0026#x2F;\u0026#x2F;everyone\u0026#x2F;\u0026#x2F; involved in its creation. Thanks!\u003c/p\u003e",
          points: null,
          parent_id: 16901783,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16903309,
          created_at: "2018-04-23T13:44:34.000Z",
          created_at_i: 1524491074,
          type: "comment",
          author: "mrtksn",
          title: null,
          url: null,
          text:
            '\u003cp\u003eBack in the day I learned Javascript to create a Firefox extension called budaneki[1] that does basically that. It got quite popular but I abandoned it later since I no longer needed and didn’t have a business model.\u003c/p\u003e\u003cp\u003eToday I’m very happy with the Safari on MacOS as you’re can preview links. I like the Wikipedia implementation a lot, but as a Safari, I’m already used to have it everywhere.\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;addons.mozilla.org\u0026#x2F;en-US\u0026#x2F;firefox\u0026#x2F;addon\u0026#x2F;budaneki\u0026#x2F;" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;addons.mozilla.org\u0026#x2F;en-US\u0026#x2F;firefox\u0026#x2F;addon\u0026#x2F;budaneki\u0026#x2F;\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901783,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902298,
      created_at: "2018-04-23T11:12:53.000Z",
      created_at_i: 1524481973,
      type: "comment",
      author: null,
      title: null,
      url: null,
      text: null,
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16905636,
      created_at: "2018-04-23T17:43:19.000Z",
      created_at_i: 1524505399,
      type: "comment",
      author: "z3t4",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThis could have been implemented entirely in the client.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901799,
      created_at: "2018-04-23T09:16:58.000Z",
      created_at_i: 1524475018,
      type: "comment",
      author: "_delirium",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI had no idea what this was talking about, and it appears to be because they\u0026#x27;ve defaulted it to off for existing logged-in users. Maybe that\u0026#x27;s a way of reducing pushback.\u003c/p\u003e\u003cp\u003eIn case you want to turn it on (or off), it\u0026#x27;s under Preferences-\u0026gt;Appearance-\u0026gt;Page previews. I think I\u0026#x27;ll probably leave it off personally. I like the previews that have already existed for a while in the mobile app, but on desktop not so sure.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16901922,
          created_at: "2018-04-23T09:46:09.000Z",
          created_at_i: 1524476769,
          type: "comment",
          author: "saagarjha",
          title: null,
          url: null,
          text:
            "\u003cp\u003e\u0026gt; they\u0026#x27;ve defaulted it to off for existing logged-in users\u003c/p\u003e\u003cp\u003eI see these even when logged out.\u003c/p\u003e",
          points: null,
          parent_id: 16901799,
          story_id: 16901599,
          children: [
            {
              id: 16901981,
              created_at: "2018-04-23T10:00:11.000Z",
              created_at_i: 1524477611,
              type: "comment",
              author: "_delirium",
              title: null,
              url: null,
              text:
                "\u003cp\u003eThat\u0026#x27;s what I meant, the previews are on by default for not-logged-in users, but appear to be off by default for logged-in users (or at least the preference was set to \u0026#x27;off\u0026#x27; for me).\u003c/p\u003e",
              points: null,
              parent_id: 16901922,
              story_id: 16901599,
              children: [
                {
                  id: 16902006,
                  created_at: "2018-04-23T10:05:43.000Z",
                  created_at_i: 1524477943,
                  type: "comment",
                  author: "saagarjha",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eI\u0026#x27;m seeing them in both cases.\u003c/p\u003e",
                  points: null,
                  parent_id: 16901981,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16903408,
                      created_at: "2018-04-23T13:57:21.000Z",
                      created_at_i: 1524491841,
                      type: "comment",
                      author: "ckoerner",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003ePerhaps you had enabled the beta feature and when the feature went to production the setting was kept enabled? Check under Preferences\u0026gt;Appearance\u0026gt;\u0026quot;Reading preferences\u0026quot;.\u003c/p\u003e",
                      points: null,
                      parent_id: 16902006,
                      story_id: 16901599,
                      children: [
                        {
                          id: 16906990,
                          created_at: "2018-04-23T20:14:56.000Z",
                          created_at_i: 1524514496,
                          type: "comment",
                          author: "saagarjha",
                          title: null,
                          url: null,
                          text:
                            "\u003cp\u003eHmm, that could be it, since it\u0026#x27;s showing up as enabled for me. I have \u0026quot;Automatically enable all new beta features\u0026quot; on, so I guess I was just betaing this feature for a long time.\u003c/p\u003e",
                          points: null,
                          parent_id: 16903408,
                          story_id: 16901599,
                          children: [],
                          options: []
                        }
                      ],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16902325,
              created_at: "2018-04-23T11:17:31.000Z",
              created_at_i: 1524482251,
              type: "comment",
              author: "blauditore",
              title: null,
              url: null,
              text:
                "\u003cp\u003eYeah, they\u0026#x27;re default-on for logged out users, but default-off for logged-in ones. Was the same for me.\u003c/p\u003e",
              points: null,
              parent_id: 16901922,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16906924,
      created_at: "2018-04-23T20:07:32.000Z",
      created_at_i: 1524514052,
      type: "comment",
      author: "starshadowx2",
      title: null,
      url: null,
      text:
        "\u003cp\u003eWikiwand has had this for years now, and with all the other improvements I still see no reason to not use it.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901817,
      created_at: "2018-04-23T09:20:28.000Z",
      created_at_i: 1524475228,
      type: "comment",
      author: "rurban",
      title: null,
      url: null,
      text:
        "\u003cp\u003eAs the major other php wiki implementor, phpwiki, I can add my input to this.\u003c/p\u003e\u003cp\u003eI\u0026#x27;ve implemented such an ajax page preview and also a page inliner (for expanding page trees via ajax) about 10 years ago. It was major work, because you essentially send a stripped down page in xml, so it needed some architectural changes to separate the various page templates properly.\nIn the end it needed 2 months work. phpwiki has a proper template design and its plugins cannot ever destroy a page layout or harm security.\u003c/p\u003e\u003cp\u003emediawiki on the other side is horribly undesigned spaghetti code, with no proper templating and plugin integration, so it needed a few years more. It\u0026#x27;s like parsing html with regexp.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16903447,
          created_at: "2018-04-23T14:02:52.000Z",
          created_at_i: 1524492172,
          type: "comment",
          author: "ckoerner",
          title: null,
          url: null,
          text:
            '\u003cp\u003e\u0026gt; mediawiki on the other side is horribly undesigned spaghetti code\u003c/p\u003e\u003cp\u003eBut like my mom\u0026#x27;s spaghetti, it\u0026#x27;s my favorite. :)\u003c/p\u003e\u003cp\u003eThink you can make it better? \n\u003ca href="https:\u0026#x2F;\u0026#x2F;wikimediafoundation.org\u0026#x2F;wiki\u0026#x2F;Work_with_us" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;wikimediafoundation.org\u0026#x2F;wiki\u0026#x2F;Work_with_us\u003c/a\u003e\u003c/p\u003e\u003cp\u003eI work for the Wikimedia Foundation, but this subtle snark is my own, and may not reflect the views of the Foundation.\u003c/p\u003e',
          points: null,
          parent_id: 16901817,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16908988,
      created_at: "2018-04-24T01:06:20.000Z",
      created_at_i: 1524531980,
      type: "comment",
      author: "akashaggarwal7",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI can\u0026#x27;t thank you enough for this feature. Great work peeps!\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16901823,
      created_at: "2018-04-23T09:21:09.000Z",
      created_at_i: 1524475269,
      type: "comment",
      author: "xab9",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThis thing is not exactly customizable.\u003c/p\u003e\u003cp\u003eI couldn\u0026#x27;t care less for the huge image in the popup layer, scrollability and extra content would be much nicer.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902009,
          created_at: "2018-04-23T10:06:20.000Z",
          created_at_i: 1524477980,
          type: "comment",
          author: "TuringTest",
          title: null,
          url: null,
          text:
            '\u003cp\u003eYou can switch to the classic Navigation popups gadget,[1] which has a lot more configuration options (including links that shows the history or whole article text in the popup).\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Tools\u0026#x2F;Navigation_popups" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Wikipedia:Tools\u0026#x2F;Navigation_pop...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16901823,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16901835,
      created_at: "2018-04-23T09:25:19.000Z",
      created_at_i: 1524475519,
      type: "comment",
      author: "sdoering",
      title: null,
      url: null,
      text:
        "\u003cp\u003eWow. Had just read that and then saw the feature first time while browsing over a list page. Was moving my curser down the list and when ever I wanted to click a link, the above link hat popped up the layer, highjacking my click and leading me to the wrong page.\u003c/p\u003e\u003cp\u003eWhat a great way of destroying the user experience with a beautifully over-engineered feature that is utterly crap while actually trying to use the underlying page.\u003c/p\u003e\u003cp\u003eThanks an awful lot for breaking the ability to use the site as I am used to and making it impossible to scan with the curser as anchor for my eyes.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16901888,
          created_at: "2018-04-23T09:38:35.000Z",
          created_at_i: 1524476315,
          type: "comment",
          author: "vbezhenar",
          title: null,
          url: null,
          text:
            "\u003cp\u003eI have a habit to select some text in a page when I\u0026#x27;m reading it. And I hate websites that trying to immediately interact with me when I\u0026#x27;m selecting a text (usually it\u0026#x27;s \u0026quot;fix a typo\u0026quot; or something similar).\u003c/p\u003e",
          points: null,
          parent_id: 16901835,
          story_id: 16901599,
          children: [
            {
              id: 16904195,
              created_at: "2018-04-23T15:23:27.000Z",
              created_at_i: 1524497007,
              type: "comment",
              author: "contoraria",
              title: null,
              url: null,
              text:
                "\u003cp\u003eHate is a pretty strong word and kind of irrational.\u003c/p\u003e\u003cp\u003eTextselection was probably not intended as reading aid, so don\u0026#x27;t be disappointed if it is abused to actually, you know, select text to do something with it.\u003c/p\u003e\u003cp\u003eI agree, user scripts can be presumptuous and what not. I used to read with text selection the same way, and even reacted repulsed at advertisement hover pop-ups. But somehow I don\u0026#x27;t do it anymore, so I don\u0026#x27;t care as much.\u003c/p\u003e",
              points: null,
              parent_id: 16901888,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16901928,
              created_at: "2018-04-23T09:47:05.000Z",
              created_at_i: 1524476825,
              type: "comment",
              author: "baud147258",
              title: null,
              url: null,
              text:
                "\u003cp\u003eSame here. I hate even more website where you can\u0026#x27;t select the text.\u003c/p\u003e",
              points: null,
              parent_id: 16901888,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16903183,
          created_at: "2018-04-23T13:26:13.000Z",
          created_at_i: 1524489973,
          type: "comment",
          author: "lakechfoma",
          title: null,
          url: null,
          text:
            "\u003cp\u003eHave you tried this in a more text based article? I was reading through something lengthy last night and I thought these pop ups were a lifesaver. They\u0026#x27;re like a TL;DR, you can avoid the context switch of actually clicking through. Sometimes even just the picture was enough to go \u0026quot;oh ok!\u0026quot;.\u003c/p\u003e\u003cp\u003eAlso personally I\u0026#x27;m prone to going down a wikipedia rabbit hole and if last night was any indication, I think these popups will help stop that.\u003c/p\u003e\u003cp\u003eSounds like you hit an edge case. Personally in that situation of a list of links I would just move the cursor outside the list. For me at least a minor change in behavior in some edge cases is worth what is otherwise a really awesome new feature.\u003c/p\u003e",
          points: null,
          parent_id: 16901835,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16901931,
          created_at: "2018-04-23T09:48:11.000Z",
          created_at_i: 1524476891,
          type: "comment",
          author: "KenanSulayman",
          title: null,
          url: null,
          text:
            "\u003cp\u003e\u0026quot;a [...] over-engineered feature that is utterly crap\u0026quot;\u003c/p\u003e\u003cp\u003eI\u0026#x27;m not usually trying to police this kind of thing, but is there a way you could have phrased this without discrediting the work of a lot of people whose goal is to help others? I\u0026#x27;m sure you\u0026#x27;d get both a little bit hurt inside and defensive when someone in your team calls your code \u0026quot;utterly crap.\u0026quot;\u003c/p\u003e\u003cp\u003eThat said, I\u0026#x27;m very sure they will be happy to hear about your feedback.\u003c/p\u003e",
          points: null,
          parent_id: 16901835,
          story_id: 16901599,
          children: [
            {
              id: 16903138,
              created_at: "2018-04-23T13:19:50.000Z",
              created_at_i: 1524489590,
              type: "comment",
              author: "neuromantik8086",
              title: null,
              url: null,
              text:
                "\u003cp\u003eIf the Wikimedia Foundation\u0026#x27;s goal was to help people, their time and money would be much better spent hiring professional \u0026#x2F; full-time editors to QC their content.  Features like this hovering preview are peripheral to their goal of providing \u003ci\u003equality\u003c/i\u003e content that can also be updated by the general public\u003c/p\u003e",
              points: null,
              parent_id: 16901931,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16903396,
              created_at: "2018-04-23T13:55:40.000Z",
              created_at_i: 1524491740,
              type: "comment",
              author: "ckoerner",
              title: null,
              url: null,
              text:
                '\u003cp\u003eAs someone who works on the team that built this feature, thank you for saying this.\u003c/p\u003e\u003cp\u003eAs for feedback, we\u0026#x27;ve had 4 years of it and welcome more. \nCheck out the FAQ and if you have something constructive to say, leave a note on the talk page.\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews#FAQ" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Page_Previews#FAQ\u003c/a\u003e\u003c/p\u003e',
              points: null,
              parent_id: 16901931,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16901854,
          created_at: "2018-04-23T09:30:30.000Z",
          created_at_i: 1524475830,
          type: "comment",
          author: "mackwerk",
          title: null,
          url: null,
          text:
            "\u003cp\u003eAt least you seem to be able to disable them, click the cog in the corner of the card :)\u003c/p\u003e",
          points: null,
          parent_id: 16901835,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16902119,
          created_at: "2018-04-23T10:32:02.000Z",
          created_at_i: 1524479522,
          type: "comment",
          author: "Sujan",
          title: null,
          url: null,
          text: "\u003cp\u003eYou’re welcome.\u003c/p\u003e",
          points: null,
          parent_id: 16901835,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16906957,
      created_at: "2018-04-23T20:11:45.000Z",
      created_at_i: 1524514305,
      type: "comment",
      author: "CM30",
      title: null,
      url: null,
      text:
        "\u003cp\u003eHave to say, I\u0026#x27;m pleased they created an algorithm to choose the right thumbnail picture here. So many other inplementations of the same idea just pick the first one on the page, and end up with someone\u0026#x27;s avatar being the featured image.\u003c/p\u003e\u003cp\u003eIndeed, as much as it took a fair bit of time, it seems the reasons behind it are all fairly logical; they actually thought carefully about the functionality and how it should work in various cases rather than just going with something that was \u0026#x27;good enough\u0026#x27; to get it out quickly.\u003c/p\u003e\u003cp\u003eNot going to complain about that.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16902100,
      created_at: "2018-04-23T10:26:56.000Z",
      created_at_i: 1524479216,
      type: "comment",
      author: "matthewfelgate",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI love the preview hovers, a Great way to quickly take in additional information.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16902868,
      created_at: "2018-04-23T12:43:41.000Z",
      created_at_i: 1524487421,
      type: "comment",
      author: "phkahler",
      title: null,
      url: null,
      text:
        "\u003cp\u003eWhen we do finally send people to Mars I think they\u0026#x27;ll appreciate having a local copy of Wikipedia with them. This would be one of the top 10 resources beyond what\u0026#x27;s required to produce air, food, water, and heat. Otherwise, something comes up and any question you have could take almost an hour to get a response from someone on earth.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16902934,
          created_at: "2018-04-23T12:52:20.000Z",
          created_at_i: 1524487940,
          type: "comment",
          author: "diggan",
          title: null,
          url: null,
          text:
            '\u003cp\u003eThat\u0026#x27;s easy (much easier than to get to Mars): Download your dump from \u003ca href="http:\u0026#x2F;\u0026#x2F;download.kiwix.org\u0026#x2F;zim\u0026#x2F;wikipedia\u0026#x2F;" rel="nofollow"\u003ehttp:\u0026#x2F;\u0026#x2F;download.kiwix.org\u0026#x2F;zim\u0026#x2F;wikipedia\u0026#x2F;\u003c/a\u003e and extract it with \u003ca href="https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;dignifiedquire\u0026#x2F;zim" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;dignifiedquire\u0026#x2F;zim\u003c/a\u003e\u003c/p\u003e\u003cp\u003eNow you have a fully static Wikipedia copy, running wherever you want it to :)\u003c/p\u003e',
          points: null,
          parent_id: 16902868,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16904096,
          created_at: "2018-04-23T15:13:39.000Z",
          created_at_i: 1524496419,
          type: "comment",
          author: "contoraria",
          title: null,
          url: null,
          text:
            "\u003cp\u003eWikipedia is, by my estimate, to 90% about animals, places, historic places, historic animals, historic people, \u0026quot;notable\u0026quot; living people, and so on. How\u0026#x27;s that going to be relevant on mars? And who\u0026#x27;s going to vet all the articles?\u003c/p\u003e",
          points: null,
          parent_id: 16902868,
          story_id: 16901599,
          children: [
            {
              id: 16904155,
              created_at: "2018-04-23T15:18:40.000Z",
              created_at_i: 1524496720,
              type: "comment",
              author: "F_r_k",
              title: null,
              url: null,
              text:
                "\u003cp\u003eMaybe, but those 1% remaining (thousands of articles) about physics, engineering, and so on are a goldmine if you are stranded without anything else.\u003c/p\u003e\u003cp\u003eIt would be dumb no to take it as the complete dump is \u0026lt; 1Tb\u003c/p\u003e",
              points: null,
              parent_id: 16904096,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902872,
      created_at: "2018-04-23T12:44:38.000Z",
      created_at_i: 1524487478,
      type: "comment",
      author: "tombrossman",
      title: null,
      url: null,
      text:
        "\u003cp\u003eOne of the best things about ad blockers is that they are really just general purpose content blockers. Want to disable this permanently? Here\u0026#x27;s an Adblock Plus-compatible filter to block the div if you find it annoying. Tested and working on uBlock Origin:\u003c/p\u003e\u003cp\u003e\u003cpre\u003e\u003ccode\u003e  ! Disable link preview popups on Wikipedia\n  en.wikipedia.org##.mwe-popups\n\u003c/code\u003e\u003c/pre\u003e\nIt\u0026#x27;s just a div with the class \u0026quot;.mwe-popups\u0026quot;, and using your ad blocker will persist the change after clearing cookies, which the preference setting (mentioned elsewhere here) does not.\u003c/p\u003e\u003cp\u003eFor Wikipedia in different languages, just change the subdomain in the filter.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16903989,
          created_at: "2018-04-23T15:03:21.000Z",
          created_at_i: 1524495801,
          type: "comment",
          author: null,
          title: null,
          url: null,
          text: null,
          points: null,
          parent_id: 16902872,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16910154,
          created_at: "2018-04-24T05:44:54.000Z",
          created_at_i: 1524548694,
          type: "comment",
          author: "tim333",
          title: null,
          url: null,
          text:
            '\u003cp\u003eOr log in and click the box in preferences \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Special:Preferences#mw-prefsection-gadgets" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Special:Preferences#mw-prefsec...\u003c/a\u003e\u003c/p\u003e',
          points: null,
          parent_id: 16902872,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16903247,
          created_at: "2018-04-23T13:35:19.000Z",
          created_at_i: 1524490519,
          type: "comment",
          author: "username223",
          title: null,
          url: null,
          text:
            "\u003cp\u003eThank you!  I found those things incredibly annoying and useless, and was just about to dig through the source to figure out how to get rid of them.\u003c/p\u003e",
          points: null,
          parent_id: 16902872,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16904625,
          created_at: "2018-04-23T16:05:40.000Z",
          created_at_i: 1524499540,
          type: "comment",
          author: "pwg",
          title: null,
          url: null,
          text:
            "\u003cp\u003eAlso, they are javascript based.  They do not appear when running NoScript set to blockk wikipedia\u0026#x27;s javascript from executing.\u003c/p\u003e",
          points: null,
          parent_id: 16902872,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16903362,
          created_at: "2018-04-23T13:50:24.000Z",
          created_at_i: 1524491424,
          type: "comment",
          author: "thought_alarm",
          title: null,
          url: null,
          text:
            "\u003cp\u003eYou can disable them by clicking the little gear icon on the pop up itself.  I figured that out by googling aroung for half an hour.\u003c/p\u003e\u003cp\u003eIt\u0026#x27;s just wretched, backwards UI all around.\u003c/p\u003e",
          points: null,
          parent_id: 16902872,
          story_id: 16901599,
          children: [
            {
              id: 16903796,
              created_at: "2018-04-23T14:44:14.000Z",
              created_at_i: 1524494654,
              type: "comment",
              author: "scrooched_moose",
              title: null,
              url: null,
              text:
                "\u003cp\u003eAs OP stated, that\u0026#x27;s tied to your cookies though and won\u0026#x27;t be a \u0026quot;permanent\u0026quot; solution.\u003c/p\u003e",
              points: null,
              parent_id: 16903362,
              story_id: 16901599,
              children: [
                {
                  id: 16904927,
                  created_at: "2018-04-23T16:31:41.000Z",
                  created_at_i: 1524501101,
                  type: "comment",
                  author: "mertd",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eI would expect the setting to persist if you\u0026#x27;re logged in but that\u0026#x27;s probably not something people do on Wikipedia.\u003c/p\u003e",
                  points: null,
                  parent_id: 16903796,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16907826,
                      created_at: "2018-04-23T21:58:34.000Z",
                      created_at_i: 1524520714,
                      type: "comment",
                      author: "azernik",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eI do, but that\u0026#x27;s only because I edit every once in a while.\u003c/p\u003e",
                      points: null,
                      parent_id: 16904927,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16903903,
          created_at: "2018-04-23T14:54:36.000Z",
          created_at_i: 1524495276,
          type: "comment",
          author: "ken",
          title: null,
          url: null,
          text:
            '\u003cp\u003eA user stylesheet can also be a content blocker, and it doesn\u0026#x27;t require a third-party extension, and it works across all domains (Wikipedia languages): \u003ca href="https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;kengruven\u0026#x2F;config\u0026#x2F;blob\u0026#x2F;master\u0026#x2F;.calm.css#L25-L26" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;kengruven\u0026#x2F;config\u0026#x2F;blob\u0026#x2F;master\u0026#x2F;.calm.css#L2...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eIt\u0026#x27;s frustrating that every popular webpage nowadays is so full of distractions that I can\u0026#x27;t use the web without blocking a lot of it.\u003c/p\u003e',
          points: null,
          parent_id: 16902872,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902622,
      created_at: "2018-04-23T12:04:50.000Z",
      created_at_i: 1524485090,
      type: "comment",
      author: null,
      title: null,
      url: null,
      text: null,
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16902373,
      created_at: "2018-04-23T11:25:24.000Z",
      created_at_i: 1524482724,
      type: "comment",
      author: "osrec",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI know some people don\u0026#x27;t appreciate it, but I really liked the preview. I feel it lets you get the gist of ancillary topics so you can understand the main article better, without having to switch contexts completely.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16907749,
      created_at: "2018-04-23T21:46:24.000Z",
      created_at_i: 1524519984,
      type: "comment",
      author: null,
      title: null,
      url: null,
      text: null,
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16907752,
      created_at: "2018-04-23T21:46:55.000Z",
      created_at_i: 1524520015,
      type: "comment",
      author: "dxt2129",
      title: null,
      url: null,
      text:
        "\u003cp\u003eReplying from Turkey, and oh boy, I can\u0026#x27;t even load the Wikipedia website.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16907837,
          created_at: "2018-04-23T22:00:34.000Z",
          created_at_i: 1524520834,
          type: "comment",
          author: "azernik",
          title: null,
          url: null,
          text: "\u003cp\u003e... joke, I assume?\u003c/p\u003e",
          points: null,
          parent_id: 16907752,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16907938,
          created_at: "2018-04-23T22:13:04.000Z",
          created_at_i: 1524521584,
          type: "comment",
          author: "jdlrobson",
          title: null,
          url: null,
          text:
            '\u003cp\u003e:( We miss you! (context: \u003ca href="https:\u0026#x2F;\u0026#x2F;ahvalnews.com\u0026#x2F;freedom-speech\u0026#x2F;wikipedia-starts-we-miss-turkey-campaign" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;ahvalnews.com\u0026#x2F;freedom-speech\u0026#x2F;wikipedia-starts-we-mis...\u003c/a\u003e)\u003c/p\u003e',
          points: null,
          parent_id: 16907752,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16901869,
      created_at: "2018-04-23T09:32:23.000Z",
      created_at_i: 1524475943,
      type: "comment",
      author: "saagarjha",
      title: null,
      url: null,
      text:
        "\u003cp\u003eI think I have these turned off, since in Safari I can Force Touch on links to preview them in a similar fashion. This has the benefit of letting \u003ci\u003eme\u003c/i\u003e choose where I want to stop reading, instead of cutting a sentence off at some arbitrary point.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16906128,
          created_at: "2018-04-23T18:32:28.000Z",
          created_at_i: 1524508348,
          type: "comment",
          author: "always_good",
          title: null,
          url: null,
          text:
            "\u003cp\u003eYou have to manually highlight phrases, even if it\u0026#x27;s already hyperlinked together, to force-click them. So I end up never using it.\u003c/p\u003e\u003cp\u003eWikipedia\u0026#x27;s is simple. And you can just disable it. Win\u0026#x2F;win.\u003c/p\u003e",
          points: null,
          parent_id: 16901869,
          story_id: 16901599,
          children: [
            {
              id: 16910561,
              created_at: "2018-04-24T07:15:32.000Z",
              created_at_i: 1524554132,
              type: "comment",
              author: "saagarjha",
              title: null,
              url: null,
              text:
                "\u003cp\u003e\u0026gt; You have to manually highlight phrases, even if it\u0026#x27;s already hyperlinked together, to force-click them\u003c/p\u003e\u003cp\u003eHuh, that\u0026#x27;s not the behavior I see with Force Touch. Hyperlinks automatically come together, and there\u0026#x27;s some sort of heuristic for detecting word clusters such as names or places.\u003c/p\u003e",
              points: null,
              parent_id: 16906128,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16907502,
      created_at: "2018-04-23T21:17:39.000Z",
      created_at_i: 1524518259,
      type: "comment",
      author: "niedzielski",
      title: null,
      url: null,
      text:
        '\u003cp\u003eHere\u0026#x27;s the code: \u003ca href="https:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;wikimedia\u0026#x2F;mediawiki-extensions-Popups" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;github.com\u0026#x2F;wikimedia\u0026#x2F;mediawiki-extensions-Popups\u003c/a\u003e.\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    },
    {
      id: 16905201,
      created_at: "2018-04-23T16:57:40.000Z",
      created_at_i: 1524502660,
      type: "comment",
      author: "mistrial9",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThis is a great collection of comments!  because it exposes a deep bias in the readers, who are mainly coders and dev-culture.\u003c/p\u003e\u003cp\u003eGuess what ?  perfectly machine-readable data is called a DATABASE, and it works well in its scope.. and if you think that all of human knowledge, history, arts and culture are perfectly represented in a DATABASE, then congratulations, you are already more computer than human in your preconceptions.\u003c/p\u003e\u003cp\u003eThere are several important layers to this onion.. lets call one \u0026quot;participation by non-specialists\u0026quot; .. a second \u0026quot;human factors, aesthetics and publishing arts\u0026quot; .. another is \u0026quot;imperfect intermediate products enable evolution\u0026quot; .. yet another is \u0026quot;taking rules before content\u0026quot; ..\u003c/p\u003e\u003cp\u003eEach topic might be an essay in itself.. Generally, I am happy to see XML essentially proposed as the answer to all human information challenges, because it takes less time to blink than to refute it, for me.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16905322,
          created_at: "2018-04-23T17:11:13.000Z",
          created_at_i: 1524503473,
          type: "comment",
          author: "yjftsjthsd-h",
          title: null,
          url: null,
          text:
            "\u003cp\u003e\u0026gt; Guess what ? perfectly machine-readable data is called a DATABASE, and it works well in its scope.. and if you think that all of human knowledge, history, arts and culture are perfectly represented in a DATABASE, then congratulations, you are already more computer than human in your preconceptions.\u003c/p\u003e\u003cp\u003eIt\u0026#x27;s on a computer. It \u003ci\u003eis\u003c/i\u003e a database. Are you also upset at people who smash the subtle beauty of music into unfeeling bits?\u003c/p\u003e",
          points: null,
          parent_id: 16905201,
          story_id: 16901599,
          children: [
            {
              id: 16905650,
              created_at: "2018-04-23T17:44:37.000Z",
              created_at_i: 1524505477,
              type: "comment",
              author: "mistrial9",
              title: null,
              url: null,
              text:
                "\u003cp\u003enot upset -- I am pleased .. many comments expose a naive point of view that I can easily contrast to .. seems fun\u003c/p\u003e",
              points: null,
              parent_id: 16905322,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16906430,
              created_at: "2018-04-23T19:07:31.000Z",
              created_at_i: 1524510451,
              type: "comment",
              author: "IAmEveryone",
              title: null,
              url: null,
              text:
                "\u003cp\u003e“Being on a computer” doesn’t define a database, just as “being a number” doesn’t define math.\u003c/p\u003e",
              points: null,
              parent_id: 16905322,
              story_id: 16901599,
              children: [],
              options: []
            },
            {
              id: 16905471,
              created_at: "2018-04-23T17:24:33.000Z",
              created_at_i: 1524504273,
              type: "comment",
              author: "PeCaN",
              title: null,
              url: null,
              text:
                "\u003cp\u003e\u0026gt;It\u0026#x27;s on a computer. It is a database.\u003c/p\u003e\u003cp\u003eSo if I print it out is it not a database anymore?\u003c/p\u003e",
              points: null,
              parent_id: 16905322,
              story_id: 16901599,
              children: [
                {
                  id: 16905675,
                  created_at: "2018-04-23T17:47:51.000Z",
                  created_at_i: 1524505671,
                  type: "comment",
                  author: "mistrial9",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003ea strong theme in the comments here is that features and amenities are harder to build upon than .. something ..  on WMF web pages because WMF internal content is not rigorously defined and enforced..  print is not the point at all, and would take considerations in yet another direction\u003c/p\u003e",
                  points: null,
                  parent_id: 16905471,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16904184,
      created_at: "2018-04-23T15:22:30.000Z",
      created_at_i: 1524496950,
      type: "comment",
      author: "publicfig",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThere\u0026#x27;s a lot of negativity in these comments (which is to be expected, as it is still HN after all) but I\u0026#x27;ve been using the preview boxes for a while now and have to say that I absolutely love them. I use Wikipedia a LOT for primative\u0026#x2F;secondary research and being able to even just figure out the dates someone lived, the very basic information, or even sometimes just a photo saves me from so many instances of new-tab opening of links that I have to remember the context of after I\u0026#x27;m done reading the passage. Really happy to see this is more available now\u003c/p\u003e\u003cp\u003eEDIT: This is unrelated, but after reading more of the comments, I legitimately can\u0026#x27;t believe how absolutely disrespectful and hateful so many of these comments are in here. I appreciate this site as a place where you can express your opinions, even if they aren\u0026#x27;t just placid support of whoever the OP is, but I really don\u0026#x27;t want to see this community dive further and further into the echo chamber of hate that it seems to be becoming.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16904972,
          created_at: "2018-04-23T16:35:50.000Z",
          created_at_i: 1524501350,
          type: "comment",
          author: "barkingcat",
          title: null,
          url: null,
          text:
            "\u003cp\u003eTo be honest, it\u0026#x27;s been like this for a long time. If you haven\u0026#x27;t noticed the negativity, you\u0026#x27;ve been reading a different \u0026quot;hacker\u0026#x27;s news\u0026quot; than everyone else on the internet.\u003c/p\u003e",
          points: null,
          parent_id: 16904184,
          story_id: 16901599,
          children: [
            {
              id: 16905980,
              created_at: "2018-04-23T18:18:40.000Z",
              created_at_i: 1524507520,
              type: "comment",
              author: "publicfig",
              title: null,
              url: null,
              text:
                "\u003cp\u003eTo be fair, I\u0026#x27;ve been a regular user under this account for 6 years now, and before that would visit frequently. I know there\u0026#x27;s always been that component to this place (it\u0026#x27;s a site publicly available on the web, it\u0026#x27;s inevitable) but it seems like there has been, maybe just through my perception, a strong rise in the ratio of valid criticism\u0026#x2F;complaints and seemingly spiteful comments that don\u0026#x27;t seem to want to accomplish anything other than to incite anger. I have always really appreciated the discussions around here!\u003c/p\u003e",
              points: null,
              parent_id: 16904972,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16904979,
          created_at: "2018-04-23T16:36:37.000Z",
          created_at_i: 1524501397,
          type: "comment",
          author: "ravenstine",
          title: null,
          url: null,
          text:
            "\u003cp\u003eIt\u0026#x27;s a very noticeable feature too.  I was very delighted to recently discover it, and it really does help Wikipedia to continue to be the revolutionary platform it is.  Kudos to those who implemented it.\u003c/p\u003e",
          points: null,
          parent_id: 16904184,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16907564,
          created_at: "2018-04-23T21:25:02.000Z",
          created_at_i: 1524518702,
          type: "comment",
          author: "yarrel",
          title: null,
          url: null,
          text:
            "\u003cp\u003eDescribing the comments here as \u0026quot;an echo chamber of hate\u0026quot; is inaccurate and serves to raise the temperature in itself.\u003c/p\u003e",
          points: null,
          parent_id: 16904184,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16905533,
          created_at: "2018-04-23T17:29:57.000Z",
          created_at_i: 1524504597,
          type: "comment",
          author: "lucideer",
          title: null,
          url: null,
          text:
            "\u003cp\u003eResponding to your edit on respectful commenting, I can\u0026#x27;t see any disrespectful comments in this thread (and generally find discourse on HN better than other places). Have some been deleted?\u003c/p\u003e\u003cp\u003eThe top-level comments are mostly positive, with one or two constructively critical ones. There are one or two sub-comments with strongly worded criticism of Wikipedia\u0026#x27;s markup (the code holding the text), but none that mention people or are in any way ad hominem.\u003c/p\u003e",
          points: null,
          parent_id: 16904184,
          story_id: 16901599,
          children: [
            {
              id: 16905833,
              created_at: "2018-04-23T18:04:04.000Z",
              created_at_i: 1524506644,
              type: "comment",
              author: "jotux",
              title: null,
              url: null,
              text:
                "\u003cp\u003eI\u0026#x27;m a little confused as well. I skimmed through the comments a second time to see if I missed something but none of the negative comments look especially disrespectful or hateful. Mostly just people complaining that they don\u0026#x27;t like pop-ups or find the feature useful.\u003c/p\u003e\u003cp\u003eThat said, I intentionally try to read text online in the most charitable voice possible, so maybe my perception is very different from others.\u003c/p\u003e",
              points: null,
              parent_id: 16905533,
              story_id: 16901599,
              children: [
                {
                  id: 16912128,
                  created_at: "2018-04-24T12:58:12.000Z",
                  created_at_i: 1524574692,
                  type: "comment",
                  author: "le-mark",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003eCharity in discourse, particularly online, is good policy for everyone[1].\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Principle_of_charity" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Principle_of_charity\u003c/a\u003e\u003c/p\u003e',
                  points: null,
                  parent_id: 16905833,
                  story_id: 16901599,
                  children: [],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16905874,
              created_at: "2018-04-23T18:07:57.000Z",
              created_at_i: 1524506877,
              type: "comment",
              author: "publicfig",
              title: null,
              url: null,
              text:
                "\u003cp\u003eI am not sure whether or not some have been deleted. When I made the edit earlier I noticed quite a few (some top level, but many secondary). I also usually find discourse on HN really a step above so many other sites I frequent! That\u0026#x27;s why it\u0026#x27;s been so disappointing to notice a rise in that kind of behavior for me lately (at least based off of my perception, of course).\u003c/p\u003e",
              points: null,
              parent_id: 16905533,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16904651,
          created_at: "2018-04-23T16:07:26.000Z",
          created_at_i: 1524499646,
          type: "comment",
          author: "agentPrefect",
          title: null,
          url: null,
          text: "\u003cp\u003eAbsolutely agree.\u003c/p\u003e",
          points: null,
          parent_id: 16904184,
          story_id: 16901599,
          children: [],
          options: []
        },
        {
          id: 16905468,
          created_at: "2018-04-23T17:24:20.000Z",
          created_at_i: 1524504260,
          type: "comment",
          author: "abalone",
          title: null,
          url: null,
          text:
            "\u003cp\u003eThere\u0026#x27;s a rule against \u003ci\u003eexcessive\u003c/i\u003e negativity but respectful, constructive criticism is an important role that HN plays here. Here\u0026#x27;s a feature that wades into what is arguably browser vendor territory, rethinking the way that hyperlinks work.[1] Is it a good pattern we should adopt throughout the web?\u003c/p\u003e\u003cp\u003eDoes its on-by-defaut nature disrupt the reading experience? Does summarizing linked content have problems? What about mobile (now approx. half of traffic and growing)?\u003c/p\u003e\u003cp\u003eI see a few comments using the word \u0026quot;hate\u0026quot; but for the most part the negative ones are just critical, with supporting points. I think a fundamental design pattern like this is worth some scrutiny alongside the support.\u003c/p\u003e\u003cp\u003e[1] Case in point: Safari implemented a feature similar to this a few years ago. It works generally across all sites, uses reserved gestures (3D Touch on mobile, 3-finger-tap on desktop) to gives users full control, and sidesteps the whole summarization problem by using more screen real estate to just show a bigger preview.\u003c/p\u003e",
          points: null,
          parent_id: 16904184,
          story_id: 16901599,
          children: [
            {
              id: 16905938,
              created_at: "2018-04-23T18:14:40.000Z",
              created_at_i: 1524507280,
              type: "comment",
              author: "publicfig",
              title: null,
              url: null,
              text:
                "\u003cp\u003eOh, I absolutely agree. I am in no way saying that constructive criticism (or hell, even warranted, non-constructive criticism sometimes) is fine and should be encouraged! The comments I was referring to (many have either been deleted or removed) were quite a bit less focused on constructive criticism than they were on just a barrage of insults towards the team responsible as if this decision was made intentionally just to spite them.\u003c/p\u003e\u003cp\u003eI think your criticism is a really good one though, especially if these link previews can be harmful towards accessibility\u0026#x2F;make basic interactions more difficult. Like I mentioned, I enabled a similar beta feature a while back that was a bit difficult to get used to, but I have grown to find indispensable. I think in the same argument, however, you can argue against any sort of hover effects on the web (drop down menus, hover animations, etc.) and to be fair, it would be hard for me to disagree with that point on many instances of that as well.\u003c/p\u003e",
              points: null,
              parent_id: 16905468,
              story_id: 16901599,
              children: [
                {
                  id: 16906989,
                  created_at: "2018-04-23T20:14:55.000Z",
                  created_at_i: 1524514495,
                  type: "comment",
                  author: "abalone",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003eMy criticism is actually about lack of mobile support. I think it\u0026#x27;s not a good practice to invest years of effort solving a problem in a way that\u0026#x27;s fundamentally incompatible with mobile. Already nearly most traffic to wikipedia is mobile, and the share is growing.[1]\u003c/p\u003e\u003cp\u003eBrowser vendors seem better positioned to solve this problem. Indeed, my reaction to this was I\u0026#x27;ve been doing this with Safari for years already (3 finger tap on macOS, light or long press on iOS). On wikipedia and all over the web. But I can see why Chrome\u0026#x2F;Firefox users would love this feature, if this is their first encounter with it.\u003c/p\u003e\u003cp\u003e[1] \u003ca href="https:\u0026#x2F;\u0026#x2F;analytics.wikimedia.org\u0026#x2F;dashboards\u0026#x2F;browsers\u0026#x2F;#all-sites-by-os" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;analytics.wikimedia.org\u0026#x2F;dashboards\u0026#x2F;browsers\u0026#x2F;#all-sit...\u003c/a\u003e\u003c/p\u003e',
                  points: null,
                  parent_id: 16905938,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16907642,
                      created_at: "2018-04-23T21:34:04.000Z",
                      created_at_i: 1524519244,
                      type: "comment",
                      author: "tbayer",
                      title: null,
                      url: null,
                      text:
                        '\u003cp\u003eMobile keeps growing indeed, but around 45% of our pageviews are still on desktop. Here\u0026#x27;s an overview on how this has changed in the last half decade: \u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Wikimedia_Audiences#\u0026#x2F;media\u0026#x2F;File:Wikimedia_monthly_pageviews_(desktop%2Bmobile),_2013-.png" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Wikimedia_Audiences#\u0026#x2F;media\u0026#x2F;Fi...\u003c/a\u003e\n(I\u0026#x27;m the data analyst who has been working on this software feature, and also keeps track of Wikimedia\u0026#x27;s reader traffic in general.)\u003c/p\u003e',
                      points: null,
                      parent_id: 16906989,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16901881,
      created_at: "2018-04-23T09:36:20.000Z",
      created_at_i: 1524476180,
      type: "comment",
      author: "Numberwang",
      title: null,
      url: null,
      text:
        '\u003cp\u003eI personally always use the mobile version\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;en.m.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Main_Page" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.m.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Main_Page\u003c/a\u003e\u003c/p\u003e\u003cp\u003efor desktop these days.\u003c/p\u003e\u003cp\u003eI much prefer it to the standard version.\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16901985,
          created_at: "2018-04-23T10:00:56.000Z",
          created_at_i: 1524477656,
          type: "comment",
          author: "simias",
          title: null,
          url: null,
          text:
            "\u003cp\u003eI hate that I don\u0026#x27;t have the bar on the left to be able to switch to a different language conveniently. I use it all the time and I\u0026#x27;ve no idea if it\u0026#x27;s even accessible at all in the mobile version. The mobile version also wastes a ton of screen real-estate but I guess that\u0026#x27;s fashionable these days.\u003c/p\u003e\u003cp\u003eI really enjoy Wikipedia but the #1 item on my feature wishlist is \u0026quot;redirect me towards the non-mobile version of the site when I click a mobile link on the desktop\u0026quot;.\u003c/p\u003e",
          points: null,
          parent_id: 16901881,
          story_id: 16901599,
          children: [
            {
              id: 16902686,
              created_at: "2018-04-23T12:17:23.000Z",
              created_at_i: 1524485843,
              type: "comment",
              author: "Symbiote",
              title: null,
              url: null,
              text:
                "\u003cp\u003eDon\u0026#x27;t worry, they\u0026#x27;ve also removed the ability to switch to other languages on the normal Wikipedia.\u003c/p\u003e\u003cp\u003eThe sidebar shows some major languages (German, Hindi etc), but not the ones I\u0026#x27;m trying to learn. Clicking the \u0026quot;more\u0026quot; button then has the most ridiculous UI ever: \u0026quot;Suggested languages\u0026quot;! What is the point of suggesting Yiddish to me?!\u003c/p\u003e",
              points: null,
              parent_id: 16901985,
              story_id: 16901599,
              children: [
                {
                  id: 16907501,
                  created_at: "2018-04-23T21:17:35.000Z",
                  created_at_i: 1524518255,
                  type: "comment",
                  author: "fireattack",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eWhat? It will remember what languages you have clicked and put them outside.\u003c/p\u003e\u003cp\u003eThis feature is a godsend, I no longer need to scroll hundreds of languages to find the one I need.\u003c/p\u003e",
                  points: null,
                  parent_id: 16902686,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16911230,
                      created_at: "2018-04-24T09:54:17.000Z",
                      created_at_i: 1524563657,
                      type: "comment",
                      author: "Symbiote",
                      title: null,
                      url: null,
                      text:
                        "\u003cp\u003eHmm, is that a recent addition? Or perhaps there was a bug.\u003c/p\u003e\u003cp\u003eI can see it sets uls-previous-languages in local storage, but if it had been working like this before today, I\u0026#x27;m sure it wouldn\u0026#x27;t have annoyed me.\u003c/p\u003e",
                      points: null,
                      parent_id: 16907501,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16902024,
              created_at: "2018-04-23T10:09:43.000Z",
              created_at_i: 1524478183,
              type: "comment",
              author: "rory096",
              title: null,
              url: null,
              text:
                "\u003cp\u003eIt\u0026#x27;s at the top of each page - click the icon directly under the article title (on the left) that looks like a Chinese character next to an A.\u003c/p\u003e\u003cp\u003eI\u0026#x27;d snip a screenshot, but I\u0026#x27;m on mobile...\u003c/p\u003e",
              points: null,
              parent_id: 16901985,
              story_id: 16901599,
              children: [
                {
                  id: 16902669,
                  created_at: "2018-04-23T12:12:30.000Z",
                  created_at_i: 1524485550,
                  type: "comment",
                  author: "simias",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003eOh! I had never paid attention to this icon. This is going to be useful in the future I\u0026#x27;m sure, thank you.\u003c/p\u003e",
                  points: null,
                  parent_id: 16902024,
                  story_id: 16901599,
                  children: [],
                  options: []
                },
                {
                  id: 16902279,
                  created_at: "2018-04-23T11:08:13.000Z",
                  created_at_i: 1524481693,
                  type: "comment",
                  author: "giancarlostoro",
                  title: null,
                  url: null,
                  text:
                    "\u003cp\u003e\u0026gt; I\u0026#x27;d snip a screenshot, but I\u0026#x27;m on mobile...\u003c/p\u003e\u003cp\u003eIf you\u0026#x27;ve got a smart phone chances are you can probably take a screenshot, only hassle then is uploading it somewhere.\u003c/p\u003e",
                  points: null,
                  parent_id: 16902024,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16906410,
                      created_at: "2018-04-23T19:04:36.000Z",
                      created_at_i: 1524510276,
                      type: "comment",
                      author: "rory096",
                      title: null,
                      url: null,
                      text:
                        '\u003cp\u003eYeah I know, I just didn\u0026#x27;t feel like going through all that while lying in bed at 4am. In any case I was thinking a cropped one would be better (hence the \u0026#x27;snip\u0026#x27;) — so here it is in all its desktop-edited glory:\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;i.imgur.com\u0026#x2F;XQKiEIp.png" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;i.imgur.com\u0026#x2F;XQKiEIp.png\u003c/a\u003e\u003c/p\u003e',
                      points: null,
                      parent_id: 16902279,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            }
          ],
          options: []
        },
        {
          id: 16902378,
          created_at: "2018-04-23T11:25:45.000Z",
          created_at_i: 1524482745,
          type: "comment",
          author: "lucideer",
          title: null,
          url: null,
          text:
            "\u003cp\u003eIf you have a Wikipedia account you can set this theme as your default theme in your account preferences without having to visit the m. domain.\u003c/p\u003e",
          points: null,
          parent_id: 16901881,
          story_id: 16901599,
          children: [
            {
              id: 16905058,
              created_at: "2018-04-23T16:42:38.000Z",
              created_at_i: 1524501758,
              type: "comment",
              author: "incogitomode",
              title: null,
              url: null,
              text:
                "\u003cp\u003eMind giving a pointer? Would love to default to the mobile view, but haven\u0026#x27;t been able to find the setting you\u0026#x27;re referring to. Google hasn\u0026#x27;t helped.\u003c/p\u003e",
              points: null,
              parent_id: 16902378,
              story_id: 16901599,
              children: [
                {
                  id: 16905557,
                  created_at: "2018-04-23T17:32:38.000Z",
                  created_at_i: 1524504758,
                  type: "comment",
                  author: "lucideer",
                  title: null,
                  url: null,
                  text:
                    '\u003cp\u003eWhen you\u0026#x27;re logged in, it\u0026#x27;s the \u0026quot;MinervaNeue\u0026quot; option here: \u003ca href="https:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Special:Preferences#mw-prefsection-rendering" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Special:Preferences#mw-prefsec...\u003c/a\u003e\u003c/p\u003e\u003cp\u003eSidenote, if—like me—you actually prefer the desktop theme, and would like the opposite: to use it on mobile. There is a mobile-friendly version of it in the works, but not yet available. You can try the in-progress version on your mobile here: \u003ca href="https:\u0026#x2F;\u0026#x2F;test.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Main_Page?mobilaction=toggle_view_desktop" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;test.wikipedia.org\u0026#x2F;wiki\u0026#x2F;Main_Page?mobilaction=toggle...\u003c/a\u003e\u003c/p\u003e',
                  points: null,
                  parent_id: 16905058,
                  story_id: 16901599,
                  children: [
                    {
                      id: 16906446,
                      created_at: "2018-04-23T19:10:29.000Z",
                      created_at_i: 1524510629,
                      type: "comment",
                      author: "jdlrobson",
                      title: null,
                      url: null,
                      text:
                        '\u003cp\u003eThat test mode of the desktop theme is actually live but super hidden away.\n\u003ca href="https:\u0026#x2F;\u0026#x2F;en.m.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=Hacker_News\u0026amp;useskin=vector" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;en.m.wikipedia.org\u0026#x2F;w\u0026#x2F;index.php?title=Hacker_News\u0026amp;use...\u003c/a\u003e\nI\u0026#x27;ve been trying to add a preference to allow user\u0026#x27;s to opt into it - \u003ca href="https:\u0026#x2F;\u0026#x2F;phabricator.wikimedia.org\u0026#x2F;T186760" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;phabricator.wikimedia.org\u0026#x2F;T186760\u003c/a\u003e - if you can a token of support there might help this get prioritised.\u003c/p\u003e',
                      points: null,
                      parent_id: 16905557,
                      story_id: 16901599,
                      children: [],
                      options: []
                    }
                  ],
                  options: []
                }
              ],
              options: []
            },
            {
              id: 16902389,
              created_at: "2018-04-23T11:28:03.000Z",
              created_at_i: 1524482883,
              type: "comment",
              author: "Numberwang",
              title: null,
              url: null,
              text: "\u003cp\u003eThank you!\u003c/p\u003e",
              points: null,
              parent_id: 16902378,
              story_id: 16901599,
              children: [],
              options: []
            }
          ],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902396,
      created_at: "2018-04-23T11:29:04.000Z",
      created_at_i: 1524482944,
      type: "comment",
      author: "mschaef",
      title: null,
      url: null,
      text:
        '\u003cp\u003eThe step ups in API usage over the last year are rather dramatic:\u003c/p\u003e\u003cp\u003e\u003ca href="https:\u0026#x2F;\u0026#x2F;grafana.wikimedia.org\u0026#x2F;dashboard\u0026#x2F;db\u0026#x2F;reading-web-page-previews?orgId=1\u0026amp;panelId=2\u0026amp;fullscreen\u0026amp;from=1480245159889\u0026amp;to=1524481960401" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;grafana.wikimedia.org\u0026#x2F;dashboard\u0026#x2F;db\u0026#x2F;reading-web-page-...\u003c/a\u003e\u003c/p\u003e',
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [
        {
          id: 16907426,
          created_at: "2018-04-23T21:10:26.000Z",
          created_at_i: 1524517826,
          type: "comment",
          author: "jdlrobson",
          title: null,
          url: null,
          text:
            '\u003cp\u003eWe\u0026#x27;re quite transparent about what we release and when - spikes can be easily attributed to events on \u003ca href="https:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Reading\u0026#x2F;Web\u0026#x2F;Release_timeline" rel="nofollow"\u003ehttps:\u0026#x2F;\u0026#x2F;www.mediawiki.org\u0026#x2F;wiki\u0026#x2F;Reading\u0026#x2F;Web\u0026#x2F;Release_timeline\u003c/a\u003e if you are interested in what caused them!\u003c/p\u003e',
          points: null,
          parent_id: 16902396,
          story_id: 16901599,
          children: [],
          options: []
        }
      ],
      options: []
    },
    {
      id: 16902142,
      created_at: "2018-04-23T10:40:04.000Z",
      created_at_i: 1524480004,
      type: "comment",
      author: "anilgulecha",
      title: null,
      url: null,
      text:
        "\u003cp\u003eThis is useful feature, but do note for some class of wikipedia surfing this is a net-negative:\u003c/p\u003e\u003cp\u003eFor the case when I came across a subject I knew not a lot about I would keep queuing them up, leading to an array of pages I\u0026#x27;d read about a topic, leading to a deeper understanding about the topic\u0026#x2F;domain. With this feature, the probability that a page would be queued up would go down.\u003c/p\u003e\u003cp\u003eSometimes going down the wiki rabbit-hole is the best form of time-sink.\u003c/p\u003e",
      points: null,
      parent_id: 16901599,
      story_id: 16901599,
      children: [],
      options: []
    }
  ],
  options: []
};
