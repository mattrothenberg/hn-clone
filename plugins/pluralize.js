// https://github.com/freearhey/vue2-filters
const toArray = (list, start) => {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret;
};

import Vue from "vue";
Vue.filter("pluralize", function(value) {
  const args = toArray(arguments, 1);
  return args.length > 1
    ? args[value % 10 - 1] || args[args.length - 1]
    : args[0] + (value === 1 ? "" : "s");
});
