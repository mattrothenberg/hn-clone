import Vue from "vue";
import { parse, distanceInWordsToNow, subDays } from "date-fns";

Vue.filter("humanize", date => {
  const parsed = parse(date);
  return `${distanceInWordsToNow(parsed)} ago`;
});
