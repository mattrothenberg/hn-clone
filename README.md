# hn-clone

> BlueVoyant Coding Challenge

## Live!

https://nameless-brushlands-81069.herokuapp.com/

## Caveat

I _think_ something funky is going on with Algolia's HN API. I've noticed that after polling the "front page" endpoint (`search?tags=front_page`) a few times, the server responds with articles from, like, years ago. Weird, man. I replicated it in a Codepen as well https://codepen.io/mattrothenberg/pen/mLPYOO.

Take a look and let me know if you see what I see.

## Build Setup

```bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
